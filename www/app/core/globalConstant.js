// prepare this module for globalConstant.
// use this as standard configuration like date format, paging size, 
// common wording, etc.


angular.module('global.constant', [])

  .constant('globalConstant', {
    'dateFormat': 'dd-MMM-yyyy',
    'dateFormatToDB':  'yyyy-MM-dd',
    'timeFormatHhMm' : 'HH:mm',
    'dateFormatHhMm' : 'dd MMM yyyy HH:mm',
    'regexNumeric': '[0-9]*(\\.[0-9]{1,2})?$',
    'regexAlphaNumeric': '[a-zA-Z,.0-9-/ ]*',
    'regexAlphabetical': '[a-zA-Z ]*',
    'currency_prefix': 'Rp. ',
  });