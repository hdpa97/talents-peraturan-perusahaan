angular
  .module("tm.controllers", [])
  .controller('ShiftCtrl', 
    function (
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var i = 0;
    var size = Main.getDataDisplaySize();

    $scope.shiftList = [];
    $scope.isLast = false;

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshShiftCtrl) {
        initMethod();
      }
      $rootScope.refreshShiftCtrl = false;

    });


    $scope.LoadMoreShiftShow = function () {
      i++;
      getListShift(i);
    };


    $scope.refresh = function () {
      initMethod();
    };

    function initMethod() {
      i = 0; // reset pagging
      $scope.shiftList = [];
      $scope.data.searchData = [];
      getListShift(0);

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");

    }

    $scope.goToAddshift = function () {
      $state.go('app.addshift', {
        'id': 0
      });
    };


    $scope.goToDetails = function (id) {
      $state.go('app.addshift', {
        'id': id
      });
    };


    function getListShift(page) {
      if (page == null || page === 'undefined') {
        page = 0;
      }

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;

      var urlApi;
      if ($scope.data.searchData == '') {
        var urlApi = Main.getUrlApi() + '/api/shift?page=' + page + '&size=' + size;
        Main.requestApi(accessToken, urlApi, successShift, $scope.errorRequest);
      } else {
        var urlApi = Main.getUrlApi() + '/api/shift/search?page=' + page + '&size=' + size + "&name=" + $scope.data.searchData;
        Main.requestApi(accessToken, urlApi, sucessSearch, $scope.errorRequest);
      }


    }

    function searchData(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/shift/search?page=' + page + '&size=' + size + "&name=" + $scope.data.searchData;

      Main.requestApi(accessToken, urlApi, sucessSearch, $scope.errorRequest);
    }

    var successShift = function (res) {
      $scope.shiftList = $scope.shiftList.concat(res.content);
      $scope.isLast = res.last;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };

    var sucessSearch = function (res) {
      $scope.shiftList = res.content;
      $scope.isLast = res.last;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };


    $scope.search = function () {
      searchData(0);
    };

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();

    });
  })


  .controller('AddShiftCtrl', 
    function (
      $stateParams,
      ionicTimePicker,
      $ionicLoading,
      $rootScope,
      $scope,
      $state, 
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    $scope.module = {};
    $scope.module.type = 'general';
    $scope.shift = {};
    var shiftId = $stateParams.id;
    $scope.indentityPage = $stateParams.id;

    $scope.defaultId = $stateParams.id;


    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    };

    var inputTime = 0;

    var timePickerOver1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;


          // $scope.shift.startTime = hourString + ":" + minuteString;
          $scope.shift.startTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: inputTime, //Optional
      // inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };


    $scope.openTimePickerOver1 = function () {

      ionicTimePicker.openTimePicker(timePickerOver1);
    };



    var timePickerOver2 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.endTime = hourString + ":" + minuteString;

          $scope.shift.endTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver2 = function () {
      ionicTimePicker.openTimePicker(timePickerOver2);
    };


    var timePickerOver3 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.breakStart = hourString + ":" + minuteString;


          $scope.shift.breakStart = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver3 = function () {
      ionicTimePicker.openTimePicker(timePickerOver3);
    };


    var timePickerOver4 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;

          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.breakEnd = hourString + ":" + minuteString;

          $scope.shift.breakEnd = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };


    $scope.openTimePickerOver4 = function () {
      ionicTimePicker.openTimePicker(timePickerOver4);
    };

    var timePickerOver5 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.lateinTime = hourString + ":" + minuteString;

          $scope.shift.lateinTime = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver5 = function () {
      ionicTimePicker.openTimePicker(timePickerOver5);
    };


    var timePickerOver6 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.lateoutTime = hourString + ":" + minuteString;


          $scope.shift.lateoutTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver6 = function () {
      ionicTimePicker.openTimePicker(timePickerOver6);
    };

    $scope.changeValue = function (value) {
      $scope.isDisabled = value;
    }

    var timePickerOver7 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.earlyinTime = hourString + ":" + minuteString;

          $scope.shift.earlyinTime = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver7 = function () {
      ionicTimePicker.openTimePicker(timePickerOver7);
    };



    var timePickerOver8 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.earlyoutTime = hourString + ":" + minuteString;


          $scope.shift.earlyoutTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver8 = function () {
      ionicTimePicker.openTimePicker(timePickerOver8);
    };



    var timePickerOver9 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.startOvertime = hourString + ":" + minuteString;

          $scope.shift.startOvertime = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver9 = function () {
      ionicTimePicker.openTimePicker(timePickerOver9);
    };



    var timePickerOver10 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.endOvertime = hourString + ":" + minuteString;

          $scope.shift.endOvertime = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver10 = function () {
      ionicTimePicker.openTimePicker(timePickerOver10);
    };





    var timePickerOver11 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.endOvertime = hourString + ":" + minuteString;

          $scope.shift.breakStart2 = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver11 = function () {
      ionicTimePicker.openTimePicker(timePickerOver11);
    };

    var timePickerOver12 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.endOvertime = hourString + ":" + minuteString;

          $scope.shift.breakEnd2 = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver12 = function () {
      ionicTimePicker.openTimePicker(timePickerOver12);
    };




    var timePickerOver13 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.endOvertime = hourString + ":" + minuteString;

          $scope.shift.breakStart3 = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver13 = function () {
      ionicTimePicker.openTimePicker(timePickerOver13);
    };




    var timePickerOver14 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          // $scope.shift.endOvertime = hourString + ":" + minuteString;

          $scope.shift.breakEnd3 = hourString + ":" + minuteString + ":" + "00";

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver14 = function () {
      ionicTimePicker.openTimePicker(timePickerOver14);
    };


    function getDetailShift() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/shift/' + shiftId;


      Main.requestApi(accessToken, urlApi, successShift, $scope.errorRequest);
    }


    var successShift = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.shift = res;
      }
    }


    $scope.saveData = function (id) {
      var startDate = $scope.shift.startTime;
      var endDate = $scope.shift.endTime;
      var breakStart = $scope.shift.breakStart;
      var breakEnd = $scope.shift.breakEnd;
      var lateinTime = $scope.shift.lateinTime;
      var lateoutTime = $scope.shift.lateoutTime;
      var earlyinTime = $scope.shift.earlyinTime;
      var earlyoutTime = $scope.shift.earlyoutTime;
      var startOvertime = $scope.shift.startOvertime;
      var endOvertime = $scope.shift.endOvertime;

      // UPDATE VALIDATION TIME FOR SHIFT 
      if (id > 0) {
        if (lateinTime != null || lateoutTime != null || earlyoutTime != null || earlyinTime != null || startOvertime != null || endOvertime != null) {
          // if (lateinTime != null || lateoutTime != null || earlyoutTime != null || earlyinTime != null) {
          // if (lateoutTime <= lateinTime) {
          //   $scope.warningAlert("Late In Time  Must Be Lower Than Late Out Time");
          // } else if (earlyoutTime <= earlyinTime) {
          //   $scope.warningAlert("Early In Time Must Be Lower Than Early Out Time");
          // } 
          // else if (endOvertime <= startOvertime) {
          //   // $scope.warningAlert("Start Overtime Must Be Lower Than End Overtime");
          // } 
          // else {
          // }
          saveDataShift(id);
        } else {
          saveDataShift(id);
        }
      } else {
        if (endDate <= startDate) {
          //   $scope.warningAlert("Time In Must Be Lower Than Time Out");
          // } else if (breakEnd <= breakStart) {
          //   $scope.warningAlert("Break Start Must Be Lower Than Break End");
          // } else if (lateoutTime <= lateinTime) {
          //   $scope.warningAlert("Late In Time Must Be Lower Than Late Out Time");
          // } else if (earlyoutTime <= earlyinTime) {
          //   $scope.warningAlert("Early In Time Must Be Lower Than Early Out Time");
          // } 
          // else if (endOvertime <= startOvertime) {
          //   // $scope.warningAlert("Start Overtime Must Be Lower Than End Overtime");
          // } 
          saveDataShift(id);
        } else {
          saveDataShift(id);
        }
      }
    }


    function verificationForm(shift) {
      if (shift.shiftCode == undefined || shift.shiftCode == '') {
        messageValidation = "Shift Code can't be empty";
        return false;
      } else if (shift.shiftName == undefined || shift.shiftName == '') {
        messageValidation = "Shift Name can't be empty";
        return false;
      } else if (shift.startTime == undefined) {
        messageValidation = "Start Time can't be empty";
        return false;
      } else if (shift.endTime == undefined) {
        messageValidation = "End Time can't be empty";
        return false;
        // } else if (shift.breakStart == undefined) {
        //   messageValidation = "Break Start can't be empty";
        //   return false;
        // } else if (shift.breakEnd == undefined) {
        //   messageValidation = "Break End can't be empty";
        //   return false;
      } else if (shift.dayType == undefined) {
        messageValidation = "Day Type can't be empty";
        return false;
      } else if (shift.earlyinTime == undefined) {
        messageValidation = "Early in time can't be empty";
        return false;
      } else if (shift.lateinTime == undefined) {
        messageValidation = "Late in time can't be empty";
        return false;
      } else if (shift.earlyoutTime == undefined) {
        messageValidation = "Early out time can't be empty";
        return false;
      } else if (shift.lateoutTime == undefined) {
        messageValidation = "Late out time can't be empty";
        return false;
      } else if (shift.dayType == undefined) {
        messageValidation = "Day Type can't be empty";
        return false;
      } else if (shift.startOvertime == undefined) {
        messageValidation = "Overtime In can't be empty";
        return false;
      }


      return true;
    }




    function saveDataShift(id) {

      if (verificationForm($scope.shift)) {

        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.shift);
        var urlApi;

        if (id > 0) {
          var urlApi = Main.getUrlApi() + '/api/shift/' + id;
          Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
        } else {
          var urlApi = Main.getUrlApi() + '/api/shift/';
          Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
        }
      } else {
        $scope.warningAlert(messageValidation);
      }


    }




    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshShiftCtrl = true;
      $scope.goBack('app.shift');

    };


    function initModule() {
      if (shiftId > 0) {
        getDetailShift();

        // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

        // if (condition1)
        //   $state.go("app.myhr");
      }
    }
    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })


  .controller('PatternCtrl', 
    function (
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var i = 0;
    var size = Main.getDataDisplaySize();
    $scope.patternList = [];



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshPatternCtrl) {
        initMethod();
      }
      $rootScope.refreshPatternCtrl = false;

    });

    $scope.LoadMorePatternShow = function () {
      i++;
      getListPattern(i);
    }


    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      i = 0; // reset pagging
      $scope.patternList = [];
      $scope.data.searchData = [];
      getListPattern(0);

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }

    $scope.goToAddpattern = function () {
      $state.go('app.addpattern', {
        'id': 0
      });
    }


    $scope.goToDetails = function (id) {
      $state.go('app.addpattern', {
        'id': id
      });
    };


    function getListPattern(page) {

      if (page == null || page === 'undefined') {
        page = 0;
      }

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi;
      if ($scope.data.searchData == '') {
        var urlApi = Main.getUrlApi() + '/api/pattern?page=' + page + '&size=' + size;
        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      } else {
        var urlApi = Main.getUrlApi() + '/api/pattern/search?page=' + page + '&size=' + size + "&name=" + $scope.data.searchData;
        Main.requestApi(accessToken, urlApi, sucessSearch, $scope.errorRequest);
      }

    }


    function searchData(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern/search?page=' + page + '&size=' + size + "&name=" + $scope.data.searchData;
      Main.requestApi(accessToken, urlApi, sucessSearch, $scope.errorRequest);
    }


    var sucessSearch = function (res) {
      $scope.patternList = res.content;
      $scope.isLast = res.last;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };

    var successRequest = function (res) {
      $scope.patternList = $scope.patternList.concat(res.content);
      $scope.isLast = res.last;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };


    $scope.search = function () {
      searchData(0);
    }



  })


  .controller('AddPatternCtrl', 
    function (
      $stateParams,
      ionicTimePicker,
      $ionicLoading,
      $rootScope, 
      $scope, 
      $state, 
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.indentityPage = $stateParams.id;


    $scope.module = {};
    $scope.module.type = 'general';
    $scope.pattern = {};
    $scope.detailPattern = {};
    $scope.listdetailPattern = {};
    var patternId = $stateParams.id;

    $scope.defaultId = $stateParams.id;


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshAddPatternCtrl) {
        initMethod();
      }
      $rootScope.refreshAddPatternCtrl = false;

    });

    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      getListDetail();
    }


    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    $scope.goToDetailPattern = function (id) {
      $state.go('app.detailpattern', {
        'id': id
      });
    }


    $scope.goToAddDetailPattern = function () {
      $state.go('app.addpatterndetail', {
        'id': patternId
      });
    }

    var timePickerOver1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.startTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver1 = function () {
      ionicTimePicker.openTimePicker(timePickerOver1);
    };



    var timePickerOver2 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.endTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver2 = function () {
      ionicTimePicker.openTimePicker(timePickerOver2);
    };


    var timePickerOver3 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.breakStart = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver3 = function () {
      ionicTimePicker.openTimePicker(timePickerOver3);
    };


    var timePickerOver4 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;

          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.breakEnd = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };


    $scope.openTimePickerOver4 = function () {
      ionicTimePicker.openTimePicker(timePickerOver4);
    };

    var timePickerOver5 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.lateinTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver5 = function () {
      ionicTimePicker.openTimePicker(timePickerOver5);
    };


    var timePickerOver6 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.lateoutTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver6 = function () {
      ionicTimePicker.openTimePicker(timePickerOver6);
    };



    var timePickerOver7 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.earlyinTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver7 = function () {
      ionicTimePicker.openTimePicker(timePickerOver7);
    };



    var timePickerOver8 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.earlyoutTime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver8 = function () {
      ionicTimePicker.openTimePicker(timePickerOver8);
    };



    var timePickerOver9 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.startOvertime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver9 = function () {
      ionicTimePicker.openTimePicker(timePickerOver9);
    };



    var timePickerOver10 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.pattern.endOvertime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver10 = function () {
      ionicTimePicker.openTimePicker(timePickerOver10);
    };


    function getDetailpattern() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern/' + patternId;


      Main.requestApi(accessToken, urlApi, successpattern, $scope.errorRequest);
    }


    var successpattern = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.pattern = res;
        Main.saveDatapatternCode(res.patternCode);
      }
    }


    function getDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/patternDetail/' + patternId;


      Main.requestApi(accessToken, urlApi, successpatternDetail, $scope.errorRequest);
    }


    var successpatternDetail = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.detailPattern = res;
      }
    }


    function getListDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern/' + patternId + '/detail';


      Main.requestApi(accessToken, urlApi, successpatternListDetail, $scope.errorRequest);
    }


    var successpatternListDetail = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.listdetailPattern = res;
      }
    }


    $scope.saveData = function (id) {

      saveDatapattern(id);

    }



    function verificationForm(pattern) {
      if (pattern.patternCode == undefined || pattern.patternCode == '') {
        messageValidation = "Pattern Code can't be empty";
        return false;
      } else if (pattern.patternName == undefined || pattern.patternName == '') {
        messageValidation = "Pattern Name can't be empty";
        return false;
      }

      return true;
    }

    function saveDatapattern(id) {

      if (verificationForm($scope.pattern)) {

        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.pattern);
        var urlApi;

        if (id > 0) {
          var urlApi = Main.getUrlApi() + '/api/pattern/' + id;
          Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
        } else {
          var urlApi = Main.getUrlApi() + '/api/pattern/';
          Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
        }
      } else {
        $scope.warningAlert(messageValidation);
      }


    }


    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshPatternCtrl = true;
      $scope.goBack('app.pattern');

    }


    function initModule() {
      if (patternId > 0) {
        getDetailpattern();
        // getListDetail();
      }

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })


  .controller('DetailPatternCtrl', 
    function (
      $stateParams, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state,
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.module = {};
    $scope.detailPattern = {};
    $scope.pattern = {};
    $scope.populateData = {};
    $scope.patternCode = {};
    $scope.shiftList = {};
    $scope.showTime = 0;
    var patternId = $stateParams.id;

    $scope.defaultId = $stateParams.id;


    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    function getDetailpattern() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/patternDetail/' + patternId;
      Main.requestApi(accessToken, urlApi, successpattern, $scope.errorRequest);
    }


    var successpattern = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.detailPattern = res;

        // 
        // 

        $scope.patternCode = res.pattern.patternCode;
        $scope.pattern.patternNo = res.patternNo;
        $scope.pattern.shiftId = res.shiftId;
        $scope.pattern.createdDate = res.createdDate;
        $scope.pattern.modifiedDate = res.modifiedDate;
        $scope.pattern.createdBy = res.createdBy;
        $scope.pattern.modifiedBy = res.modifiedBy;

        $scope.populateData = {
          id: res.shiftId,
          startTime: res.shift.startTime,
          endTime: res.shift.endTime
        };
      }
    }



    function getListShift() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/shift';


      Main.requestApi(accessToken, urlApi, successShift, $scope.errorRequest);
    }


    var successShift = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.shiftList = res.content;
        // Main.saveListShiftPattern([res.content]);
      }



    }


    $scope.getData = function (data) {
      $scope.pattern.shiftId = data.id;

      $scope.showTime = 1;
    }

    $scope.saveData = function (id) {

      saveDatapattern(id);

    }


    function verificationForm(pattern) {
      if (pattern.patternNo == undefined || pattern.patternNo == '') {
        messageValidation = "Pattern No can't be empty";
        return false;
      } else if (pattern.shiftId == undefined || pattern.shiftId == '') {
        messageValidation = "Shift Id can't be empty";
        return false;
      }

      return true;
    }




    function saveDatapattern(id) {
      if (verificationForm($scope.pattern)) {
        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.pattern);



        var urlApi = Main.getUrlApi() + '/api/patternDetail/' + id;
        Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
      } else {
        $scope.warningAlert(messageValidation);
      }

    }


    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAddPatternCtrl = true;

      $scope.goBack('app.pattern');


    }


    function initModule() {
      getDetailpattern();
      getListShift();
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })


  .controller('AddPatternDetailCtrl', 
    function (
      $stateParams,
      $ionicLoading,
      $rootScope, 
      $scope, 
      $state, 
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.module = {};
    $scope.detailPattern = {};
    $scope.pattern = {};
    $scope.populateData = 0;
    $scope.patternCode = {};
    $scope.shiftList = {};
    var patternId = $stateParams.id;
    $scope.defaultId = $stateParams.id;

    $scope.patternCode = Main.getDatapatternCode();


    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    };


    function getListShift() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/shift';


      Main.requestApi(accessToken, urlApi, successShift, $scope.errorRequest);
    }


    var successShift = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.shiftList = res.content;


        // $scope.populateData = {id:res.shiftId, startTime: res.startTime, endTime: res.endTime };
      }
    }



    function getDetailpattern() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/patternDetail/' + patternId;
      Main.requestApi(accessToken, urlApi, successpattern, $scope.errorRequest);
    }

    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }


    var successpattern = function (res) {

      if (!isEmpty(res)) {
        $ionicLoading.hide();
        $scope.patternCode = res.pattern.patternCode;
      }
    }

    $scope.getData = function (data) {
      $scope.pattern.shiftId = data;

      // var dataListShift = $scope.shiftList;




      // $scope.populateData = [];

      // var toSearch = data;

      // for(var i=0; i < dataListShift.length; i++) {
      //   for(key in dataListShift[i]) {
      //     if(dataListShift[i][key].indexOf(toSearch)!=-1) {
      //       populateData.push(dataListShift[i]);
      //     }
      //   }
      // }
    }

    $scope.saveData = function (id) {
      saveDatapattern(id);
    }

    function verificationForm(pattern) {
      if (pattern.patternNo == undefined || pattern.patternNo == '') {
        messageValidation = "Pattern No can't be empty";
        return false;
      } else if (pattern.shiftId == undefined || pattern.shiftId == '') {
        messageValidation = "Shift Id can't be empty";
        return false;
      }

      return true;
    }




    function saveDatapattern(id, shiftid) {
      if (verificationForm($scope.pattern)) {
        var patternId = $stateParams.id;
        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.pattern);


        var urlApi = Main.getUrlApi() + '/api/pattern/' + patternId + '/detail';
        Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

        // getDetailpattern();
      } else {
        $scope.warningAlert(messageValidation);
      }
    }


    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAddPatternCtrl = true;

      $scope.goBack('app.pattern');


    }

    function initModule() {
      // getDetailpattern();
      getListShift();
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  .controller('AttendancegroupCtrl', 
    function (
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main
    ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.attendanceGroupList = [];
    $scope.isShow = 1;

    var i = 0;
    var sizeData = 5;
    $scope.isLoadMoreAtShow = false;



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshAttendancegroupCtrl) {
        initMethod();
      }
      $rootScope.refreshAttendancegroupCtrl = false;

    });

    // $scope.searchName = function(event){
    //   if (event.which === 13)
    //     find(0);
    // }

    $scope.LoadMoreAtShow = function () {
      i++;

      getListattendanceGroup(i);
    }


    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      i = 0; // reset pagegging
      getListattendanceGroupRef();

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }


    $scope.goToAddattendanceGroup = function () {
      $state.go('app.addattendancegroup', {
        'id': 0
      });
    }


    $scope.goToDetails = function (id) {
      $state.go('app.addattendancegroup', {
        'id': id
      });
    };





    function getListattendanceGroup(page) {

      if (page == null || page === 'undefined') {
        page = 0;
      }

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup?page=' + page + '&size=' + sizeData;


      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    }

    $scope.formSearching = {
      search: ""
    };
    $scope.requests = [];
    $scope.actionFind = false;

    var h = 0; //for attendance
    var size = Main.getDataDisplaySize();

    var successFind = function (res) {
      $ionicLoading.hide();
      if (res.content != null) {
        $scope.attendanceGroupList = res.content;
      } else {
        $scope.attendanceGroupList = res;
      }



      if (res.last == false) {
        $scope.isLoadMoreAtShow = true;
      } else {
        $scope.isLoadMoreAtShow = false;
      }


      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }


    $scope.find = function (page) {
      $scope.isShow = 0;
      $scope.actionFind = true;
      var searchText = $scope.formSearching.search;
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      if ($scope.formSearching.selectby == 'Code') {
        var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?code=' + $scope.formSearching.search + '&page=' + 0 + '&size=' + sizeData;
      } else if ($scope.formSearching.selectby == '') {
        var urlApi = Main.getUrlApi() + '/api/attendanceGroup';
      } else {
        var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?name=' + $scope.formSearching.search + '&page=' + 0 + '&size=' + sizeData;
      }


      Main.requestApi(accessToken, urlApi, successFind, $scope.errorRequest);
    }


    var successattendanceGroup = function (res) {
      $scope.isLoadMoreAtShow = false;
      $ionicLoading.hide();
      if (res != null) {

        for (var i = 0; i < res.content.length; i++) {
          var obj = res.content[i];

          $scope.attendanceGroupList.push(obj);

          if (res.last == false) {
            $scope.isLoadMoreAtShow = true;
          } else {
            $scope.isLoadMoreAtShow = false;
          }


          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        }
      }
    }




    function getListattendanceGroupRef(page) {

      if (page == null || page === 'undefined') {
        page = 0;
      }

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup?page=' + page + '&size=' + sizeData;


      Main.requestApi(accessToken, urlApi, successattendanceGroupRef, $scope.errorRequest);
    }



    var successattendanceGroupRef = function (res) {
      $scope.isLoadMoreAtShow = false;
      $ionicLoading.hide();
      if (res != null) {



        $scope.attendanceGroupList = res.content;

        if (res.last == false) {
          $scope.isLoadMoreAtShow = true;
        } else {
          $scope.isLoadMoreAtShow = false;
        }


        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      }
    }

    // function initModule() {
    //   // getListattendanceGroup();
    // }

    // initModule();





  })


  .controller('AddAttendancegroupCtrl', 
    function (
      $stateParams, 
      ionicTimePicker, 
      $filter, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main
    ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.module = {};
    $scope.module.type = 'general';
    $scope.attendanceGroup = {};
    $scope.detailattendanceGroup = {};
    $scope.listdetailattendanceGroup = {};
    $scope.toggled = {};
    var attendanceGroupId = $stateParams.id;

    // var g
    $scope.defaultId = $stateParams.id;

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshAddAttendancegroupCtrl) {
        initMethod();
      }
      $rootScope.refreshAddAttendancegroupCtrl = false;

    });

    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      getListDetail();
      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if(condition1)
      //   $state.go("app.myhr");
    }


    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    $scope.goToDetailattendancegroup = function (id, patternId) {
      $state.go('app.detailattendancegroup', {
        'id': id,
        'patternId': patternId
      });
    }

    // nih
    $scope.goToAddDetailAd = function () {
      // $state.go('app.addattendancegroupdetail', { 'id': 58 });
      $state.go('app.addattendancegroupdetail', {
        'id': attendanceGroupId
      });
    }

    var timePickerOver10 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {
          // 
        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.attendanceGroup.endOvertime = hourString + ":" + minuteString + ":" + "00";
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver10 = function () {
      ionicTimePicker.openTimePicker(timePickerOver10);
    };


    function getDetailattendanceGroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + attendanceGroupId;


      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    }


    var successattendanceGroup = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.attendanceGroup = res;
        Main.saveDataattendanceGroupCode(res.groupCode);
        Main.saveDataattendanceGroupName(res.groupName);
        Main.saveDataattendanceGroupWorkingHours(res.workingHours);
        Main.saveDataattendanceGroupHoliday(res.isWorkingOnHoliday);
      }
    }


    function getDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + attendanceGroupId;


      Main.requestApi(accessToken, urlApi, successattendanceGroupDetail, $scope.errorRequest);
    }


    var successattendanceGroupDetail = function (res) {

      if (res != null) {
        $ionicLoading.hide();
        $scope.detailattendanceGroup = res;

      }
    }

    function getListDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + attendanceGroupId + '/detail';


      Main.requestApi(accessToken, urlApi, successattendanceGroupListDetail, $scope.errorRequest);
    }


    var successattendanceGroupListDetail = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.listdetailattendanceGroup = res;
      }
    }


    $scope.indentityPage = $stateParams.id;

    $scope.saveData = function (id) {
      var startDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveStartDate), 'yyyy-MM-dd');
      var endDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveEndDate), 'yyyy-MM-dd');
      saveDataattendanceGroup(id);
    }

    var isToggled = function () {
      if ($scope.toggled.checked) $scope.attendanceGroup.isWorkingOnHoliday = true;
      else if (!$scope.toggled.checked) $scope.attendanceGroup.isWorkingOnHoliday = false;
    };

    function verificationForm(attendanceGroup) {
      if (attendanceGroup.groupCode == undefined || attendanceGroup.groupCode == '') {
        messageValidation = "Code can't be empty.";
        return false;
      } else if (attendanceGroup.groupName == undefined || attendanceGroup.groupName == '') {
        messageValidation = "Name can't be empty.";
        return false;
      } else if (attendanceGroup.workingHours == undefined || attendanceGroup.workingHours == '') {
        messageValidation = "Working Hours can't be empty.";
        return false;
      } else {
        return true;
      }
    }

    function saveDataattendanceGroup(id) {
      $scope.detailattendanceGroup.effectiveStartDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveStartDate), 'yyyy-MM-dd');

      $scope.detailattendanceGroup.effectiveEndDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveEndDate), 'yyyy-MM-dd');

      if ($scope.toggled.checked == undefined || $scope.toggled.checked == null) $scope.attendanceGroup.isWorkingOnHoliday = false;
      else if ($scope.toggled.checked) $scope.attendanceGroup.isWorkingOnHoliday = true;
      else if (!$scope.toggled.checked) $scope.attendanceGroup.isWorkingOnHoliday = false;

      if (verificationForm($scope.attendanceGroup)) {

        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.attendanceGroup);
        var urlApi;

        if (id > 0) {
          var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + id;
          Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

        } else {
          var urlApi = Main.getUrlApi() + '/api/attendanceGroup/';
          Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

        }
      } else {
        $scope.warningAlert(messageValidation);
      }



    }


    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goTo('app.attendancegroup');
      // $route.reload();

    }


    function initModule() {
      if (attendanceGroupId > 0) {
        getDetailattendanceGroup();
        // getListDetail();
      }
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  .controller('DetailAttendancegroupCtrl', 
    function (
      $stateParams, 
      ionicDatePicker, 
      $filter, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.module = {};
    $scope.detailattendanceGroup = {};
    $scope.attendanceGroup = {};
    $scope.populateData = {};
    $scope.populateDataPatternDetail = {};
    $scope.attendanceGroupCode = {};
    $scope.shiftList = {};
    $scope.patternList = {};
    $scope.attendanceGroupName = {};
    var attendanceGroupId = $stateParams.id;
    $scope.defaultId = $stateParams.id;
    $scope.defaultPatternId = $stateParams.patternId;
    var dataPatternId = 0;
    $scope.patternDetail = 0;
    $scope.patternIdGlobal = 0;
    $scope.patternIdGlobalDetail = {};
    $scope.isShowAfterSelect = 0;
    $scope.startformPatternDetail = 0;
    $scope.startformPatternDetailData = 0;

    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    function getDetailattendanceGroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;

      var urlApi = Main.getUrlApi() + '/api/attendanceGroupDetail/' + attendanceGroupId;
      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);


      getDetailpattern();
    }


    var successattendanceGroup = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.detailattendanceGroup = res;
        $scope.attendanceGroupName = res.group.groupName;
        $scope.populateData = {
          id: res.patternDtl.patternId
        };
        $scope.patternIdGlobal = res.patternDtl.patternId;
        $scope.startformPatternDetailData = res.patternDtl.id;


        $scope.populateDataPatternDetail = {
          patternNo: res.patternDtl.patternNo
        };
      }
    }


    var size = 500;

    function getPattern() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern?size=' + size;
      Main.requestApi(accessToken, urlApi, successpattern, $scope.errorRequest);
    }


    var successpattern = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.patternList = res.content;
      }
    }



    function getListShift() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/shift';


      Main.requestApi(accessToken, urlApi, successShift, $scope.errorRequest);
    }


    var successShift = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.shiftList = res;
      }
    }


    $scope.getData = function (data) {
      $scope.detailattendanceGroup.patternId = data.id;
      $scope.patternIdGlobal = data.id;
      $scope.isShowAfterSelect = 1;
      getDetailpattern();



    }


    $scope.getDataDetail = function (data) {
      // alert(data.patternId);
      $scope.detailattendanceGroup.patternDtlId = data.id;
      $scope.detailattendanceGroup.patternId = data.patternId;
      // $scope.deta
    }


    function getDetailpattern() {


      var dataPatternId = $scope.patternIdGlobal;

      if (dataPatternId == null || dataPatternId == '' || dataPatternId == 0) {

        var dataPatternId = $scope.defaultPatternId;
      }



      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern/' + dataPatternId + '/detail';


      Main.requestApi(accessToken, urlApi, successpatternDetailData, $scope.errorRequest);
    }


    var successpatternDetailData = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.patternDetail = res;
      }
    }


    $scope.saveData = function (id) {
      var startDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveStartDate), 'yyyy-MM-dd');
      var endDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveEndDate), 'yyyy-MM-dd');

      saveDataattendanceGroup(id);
    }


    function verificationForm(detailattendanceGroup) {
      if (detailattendanceGroup.patternId == undefined || detailattendanceGroup.patternId == '') {
        messageValidation = "Pattern can't be empty";
        return false;
      } else if (detailattendanceGroup.patternDtlId == undefined || detailattendanceGroup.patternDtlId == '') {
        messageValidation = "Start From Pattern can't be empty";
        return false;
      } else if (detailattendanceGroup.effectiveStartDate == undefined || detailattendanceGroup.effectiveStartDate == 'Invalid Date') {
        messageValidation = "Effective Start Date can't be empty";
        return false;
      } else if (detailattendanceGroup.effectiveEndDate == undefined || detailattendanceGroup.effectiveEndDate == 'Invalid Date') {
        messageValidation = "Effective End Date can't be empty";
        return false;
      }

      return true;
    }


    function saveDataattendanceGroup(id) {
      $scope.detailattendanceGroup.effectiveStartDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveStartDate), 'yyyy-MM-dd');

      $scope.detailattendanceGroup.effectiveEndDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveEndDate), 'yyyy-MM-dd');

      if (verificationForm($scope.detailattendanceGroup)) {
        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.detailattendanceGroup);




        var urlApi = Main.getUrlApi() + '/api/attendanceGroupDetail/' + id;
        Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
      } else {
        $scope.warningAlert(messageValidation);
      }

    }

    var datepicker1 = {
      callback: function (val) { //Mandatory
        // 
        $scope.detailattendanceGroup.effectiveStartDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };



    var datepicker2 = {
      callback: function (val) { //Mandatory
        // 
        $scope.detailattendanceGroup.effectiveEndDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker2 = function () {
      ionicDatePicker.openDatePicker(datepicker2);
    };




    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAddAttendancegroupCtrl = true;
      $scope.goBack('app.attendancegroup');


    }


    function initModule() {
      getDetailattendanceGroup();
      getListShift();
      getPattern();
      getDetailpattern();
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })



  .controller('AddAttendancegroupDetailCtrl', 
    function (
      $stateParams, 
      ionicDatePicker, 
      $filter, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main
      ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    // 
    // 

    $scope.module = {};
    $scope.detailattendanceGroup = {};
    $scope.attendanceGroup = {};
    $scope.populateData = 0;
    $scope.patternIdGlobal = 0;
    $scope.attendanceGroupName = {};
    $scope.attendanceGroupName = Main.getDataattendanceGroupCode();
    $scope.shiftList = {};

    $scope.detailattendanceGroup.effectiveEndDate = '4000-12-31';
    var attendanceGroupId = $stateParams.id;
    $scope.defaultId = $stateParams.id;


    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    function getDetailattendanceGroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroupDetail/' + attendanceGroupId;
      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    }


    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }


    var successattendanceGroup = function (res) {
      $ionicLoading.hide();
      if (!isEmpty(res)) {

        // $scope.attendanceGroupName = res.group.groupName;
        $scope.attendanceGroupName = Main.getDataattendanceGroupName();

      }
      // nih 2
    }

    $scope.getData = function (data) {
      $scope.attendanceGroup.shiftId = data;
    }

    $scope.saveData = function (id) {
      var startDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveStartDate), 'yyyy-MM-dd');
      var endDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveEndDate), 'yyyy-MM-dd');


      if (endDate <= startDate) {
        $scope.warningAlert("Efective Start Date Must Be Lower Than Efective End Date");
      } else {
        saveDataattendanceGroup(id);
      }
    }



    function verificationForm(detailattendanceGroup) {
      if (detailattendanceGroup.patternId == undefined || detailattendanceGroup.patternId == '') {
        messageValidation = "Pattern can't be empty";
        return false;
      } else if (detailattendanceGroup.patternDtlId == undefined || detailattendanceGroup.patternDtlId == '') {
        messageValidation = "Start From Pattern can't be empty";
        return false;
      } else if (detailattendanceGroup.effectiveStartDate == undefined || detailattendanceGroup.effectiveStartDate == 'Invalid Date') {
        messageValidation = "Effective Start Date can't be empty";
        return false;
      } else if (detailattendanceGroup.effectiveEndDate == undefined || detailattendanceGroup.effectiveEndDate == 'Invalid Date') {
        messageValidation = "Effective End Date can't be empty";
        return false;
      }

      return true;
    }

    function saveDataattendanceGroup(id, shiftid) {
      $scope.detailattendanceGroup.effectiveStartDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveStartDate), 'yyyy-MM-dd');
      $scope.detailattendanceGroup.effectiveEndDate = $filter('date')(new Date($scope.detailattendanceGroup.effectiveEndDate), 'yyyy-MM-dd');
      var attendanceGroupId = $stateParams.id;

      if (verificationForm($scope.detailattendanceGroup)) {
        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.detailattendanceGroup);


        var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + attendanceGroupId + '/detail';
        Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

        getDetailattendanceGroup();
      } else {
        $scope.warningAlert(messageValidation);
      }
    }


    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAddAttendancegroupCtrl = true;
      $scope.goBack('app.attendancegroup');


    }

    var datepicker1 = {
      callback: function (val) { //Mandatory
        // 
        $scope.detailattendanceGroup.effectiveStartDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };



    var datepicker2 = {
      callback: function (val) { //Mandatory
        // 
        $scope.detailattendanceGroup.effectiveEndDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker2 = function () {
      ionicDatePicker.openDatePicker(datepicker2);
    };



    function getPattern() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern';
      Main.requestApi(accessToken, urlApi, successpattern, $scope.errorRequest);
    }


    var successpattern = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.patternList = res.content;
      }
    }

    function getDetailpattern() {


      var dataPatternId = $scope.patternIdGlobal;


      if (dataPatternId == null || dataPatternId == '' || dataPatternId == 0) {


        var dataPatternId = $scope.defaultPatternId;
      }



      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern/' + dataPatternId + '/detail';


      Main.requestApi(accessToken, urlApi, successpatternDetailData, $scope.errorRequest);
    }


    var successpatternDetailData = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.patternDetail = res;
      }
    }


    $scope.getData = function (data) {
      $scope.detailattendanceGroup.patternId = data.id;
      $scope.patternIdGlobal = data.id;
      $scope.isShowAfterSelect = 1;
      getDetailpattern();



    }


    $scope.getDataDetail = function (data) {
      $scope.detailattendanceGroup.patternDtlId = data.id;
    }

    function initModule() {
      getDetailattendanceGroup();

      getPattern();
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  // assign attendance

  .controller('AssignAttendancegroupCtrl', 
    function (
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main
    ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var i = 0;
    var dataSize = 5;
    $scope.isLoadMoreAatShow = false;

    var npk = Main.getDataEmployeeNpk();

    $scope.empNpk = Main.getDataEmployeeNpk();
    $scope.empName = Main.getDataEmployee();

    $scope.attendanceGroupList = {};

    $scope.assignAttendanceGroupList = [];
    $scope.isShow = 1;



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      // if (data.direction != undefined && data.direction != 'back')
      //   initMethod();

      // if ($rootScope.refreshAssignAttendancegroupCtrl) {
      //   initMethod();
      // }

      initMethod();
      $rootScope.refreshAssignAttendancegroupCtrl = false;



    });



    $scope.LoadMoreAatShow = function () {
      i++;
      // 

      // 
      getListassignattendancegroup(i);
    }


    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      i = 0; // reset pagging
      getListassignattendancegroupRef();
    }


    $scope.goToAddattendanceGroup = function () {
      $state.go('app.addassignattendancegroup', {
        'id': 0
      });
    }


    $scope.goToDetails = function (id) {
      $state.go('app.addassignattendancegroup', {
        'id': id
      });
    };


    function getListassignattendancegroup(page) {
      if (page == null || page === 'undefined') {
        page = 0;
      }


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/empGroup/findByEmployee/' + npk + '?page=' + page + '&size=' + dataSize;


      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    }



    function getListassignattendancegroupRef(page) {
      if (page == null || page === 'undefined') {
        page = 0;
      }


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/empGroup/findByEmployee/' + npk + '?page=' + page + '&size=' + dataSize;


      Main.requestApi(accessToken, urlApi, successattendanceGroupRef, $scope.errorRequest);
    }



    // function getLoadassignattendancegroup() {
    //   $ionicLoading.show({
    //     template: '<ion-spinner></ion-spinner>'
    //   });

    //   var accessToken = Main.getSession("token").access_token;
    //   var urlApi = Main.getUrlApi() + '/api/empGroup/findByEmployee/' + npk;


    //   Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    // }





    $scope.formSearching = {
      search: ""
    };
    $scope.requests = [];
    $scope.actionFind = false;

    var h = 0; //for attendance
    var size = Main.getDataDisplaySize();

    // var successFind = function(res) {
    //   $ionicLoading.hide();
    //   if(res.content != null){
    //     $scope.attendanceGroupList = res.content;
    //   }else{
    //     $scope.attendanceGroupList = res;
    //   }

    //   $ionicLoading.hide();
    //   $scope.$broadcast('scroll.refreshComplete');
    //   $scope.$broadcast('scroll.infiniteScrollComplete');
    // }


    // $scope.find = function(page) {
    //   $scope.isShow = 0;
    //   $scope.actionFind = true;
    //   var searchText = $scope.formSearching.search;
    //   $ionicLoading.show({
    //     template: '<ion-spinner></ion-spinner>'
    //   });

    //   var accessToken = Main.getSession("token").access_token;
    //   if ($scope.formSearching.selectby == 'Code') {
    //     var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?code=' + $scope.formSearching.search;
    //   }else if($scope.formSearching.selectby == ''){
    //      var urlApi = Main.getUrlApi() + '/api/attendanceGroup';
    //   }else {
    //     var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?name=' + $scope.formSearching.search;
    //   }


    //   Main.requestApi(accessToken, urlApi, successFind, $scope.errorRequest);
    // }


    var successattendanceGroup = function (res) {
      $scope.isLoadMoreAatShow = false;
      $ionicLoading.hide();

      if (res != null) {




        if (res[0] != null) {
          Main.saveDataEmployeeId(res[0].employeeId);
        }



        for (var i = 0; i < res.content.length; i++) {
          var obj = res.content[i];

          $scope.assignAttendanceGroupList.push(obj);

          if (res.last == false) {
            $scope.isLoadMoreAatShow = true;
          } else {
            $scope.isLoadMoreAatShow = false;
          }


          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        }
      }
    }


    var successattendanceGroupRef = function (res) {
      $scope.isLoadMoreAatShow = false;
      $ionicLoading.hide();

      if (res != null) {



        if (res[0] != null) {
          Main.saveDataEmployeeId(res[0].employeeId);
        }




        $scope.assignAttendanceGroupList = res.content;

        if (res.last == false) {
          $scope.isLoadMoreAatShow = true;
        } else {
          $scope.isLoadMoreAatShow = false;
        }


        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');

      }
    }





  })

  .controller('AssignAttendancegroupbygroupCtrl', 
    function (
      $stateParams, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state,
      Main
    ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var groupCode = $stateParams.groupCode;

    $scope.groupCode = Main.getGroupCode();
    $scope.groupName = Main.getGroupName();

    // 

    $scope.attendanceGroupList = {};
    $scope.isShow = 1;



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshAttendancegroupCtrl) {
        initMethod();
      }
      $rootScope.refreshAttendancegroupCtrl = false;



    });


    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      getListassignattendancegroup();


    }


    $scope.goToAddattendanceGroup = function () {
      $state.go('app.addassignattendancegroup', {
        'id': 0
      });
    }


    $scope.goToDetails = function (id) {
      $state.go('app.addassignattendancegroup', {
        'id': id
      });
    };


    function getListassignattendancegroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/empGroup/findByGroup/' + groupCode;


      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    }


    $scope.formSearching = {
      search: ""
    };
    $scope.requests = [];
    $scope.actionFind = false;

    var h = 0; //for attendance
    var size = Main.getDataDisplaySize();

    // var successFind = function(res) {
    //   $ionicLoading.hide();
    //   if(res.content != null){
    //     $scope.attendanceGroupList = res.content;
    //   }else{
    //     $scope.attendanceGroupList = res;
    //   }

    //   $ionicLoading.hide();
    //   $scope.$broadcast('scroll.refreshComplete');
    //   $scope.$broadcast('scroll.infiniteScrollComplete');
    // }


    // $scope.find = function(page) {
    //   $scope.isShow = 0;
    //   $scope.actionFind = true;
    //   var searchText = $scope.formSearching.search;
    //   $ionicLoading.show({
    //     template: '<ion-spinner></ion-spinner>'
    //   });

    //   var accessToken = Main.getSession("token").access_token;
    //   if ($scope.formSearching.selectby == 'Code') {
    //     var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?code=' + $scope.formSearching.search;
    //   }else if($scope.formSearching.selectby == ''){
    //      var urlApi = Main.getUrlApi() + '/api/attendanceGroup';
    //   }else {
    //     var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?name=' + $scope.formSearching.search;
    //   }


    //   Main.requestApi(accessToken, urlApi, successFind, $scope.errorRequest);
    // }


    var successattendanceGroup = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.assignAttendanceGroupList = res;
        Main.saveDataEmployeeId(res[0].employeeId);
      }
    }

    function initModule() {
      getListassignattendancegroup();
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });



  })

  .controller('AssignAttendancegroupSearchCtrl', 
    function (
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main, 
      $ionicModal, 
      $ionicPopup
    ) {
    
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.goToList = function () {
      $state.go("app.assignattendancegroup");
      $scope.closeModal(1);
      $scope.closeModal(2);
      $scope.closeModal(11);
      $scope.closeModal(22);
      $scope.closeModal(33);
      $scope.closeModal(44);
      $ionicLoading.hide();
    }


    $scope.goToListEmployee = function (npk, empName, employeeId) {

      Main.saveEmployee(empName); // setter
      Main.saveDataEmployeeNpk(npk); // setter
      Main.saveDataEmployeeId(employeeId); // setter 
      $state.go('app.assignattendancegroup', {
        'npk': npk
      });
      $scope.closeModal(1);
      $scope.closeModal(2);
      $scope.closeModal(11);
      $scope.closeModal(22);
      $scope.closeModal(33);
      $scope.closeModal(44);
      $ionicLoading.hide();
    }


    $scope.goToListGroup = function (groupCode, groupName) {
      Main.saveGroupCode(groupCode); // setter
      Main.saveGroupName(groupName); // setter
      $state.go('app.assignattendancegroupbygroup', {
        'groupCode': groupCode,
        'groupName': groupName
      });
      $scope.closeModal(1);
      $scope.closeModal(2);
      $scope.closeModal(11);
      $scope.closeModal(22);
      $scope.closeModal(33);
      $scope.closeModal(44);
      $ionicLoading.hide();
    }


    var infiniteLimit = 15;
    $scope.approvalType = {};
    $scope.requestHeader = {
      npk: '',
      empName: ''
    };
    $scope.isLoadMoreShow = false;
    $scope.findStatus = false;
    $scope.nameItems = [];
    $scope.npkItems = [];
    $scope.groupCodeItems = [];
    $scope.groupNameItems = [];
    $scope.items = [];
    // var size = Main.getDataDisplaySize();
    var size = 1;
    var j = 0;

    $scope.loadMoreName = function () {
      // $scope.infiniteLimit += $scope.infiniteLimit;
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      j++;
      // 
      $scope.searchByName(j);
    };

    $scope.loadPrevName = function () {
      j--;
      // 
      $scope.searchByName(j);
    }

    $scope.loadMoreNpk = function () {
      // $scope.infiniteLimit += $scope.infiniteLimit;
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      j++;
      // 
      $scope.searchByNpk(j);
    };

    $scope.loadPrevNpk = function () {
      // $scope.infiniteLimit += $scope.infiniteLimit;
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      j--;
      // 
      $scope.searchByNpk(j);
    };

    // $scope.loadMoreGroupCode = function (){
    //   // $scope.infiniteLimit += $scope.infiniteLimit;
    //   // $scope.$broadcast('scroll.infiniteScrollComplete');
    //   j++;
    //   // 
    //   $scope.searchByGroupCode(j);
    // };



    $scope.findByName = {
      search: ""
    };
    $scope.searchByName = function (page) {
      $scope.findStatus = false;
      if ($scope.findByName.search == null || $scope.findByName.search == '')
        $ionicPopup.show({
          title: "Text field can't be empty!",
          buttons: [{
            text: 'OK'
          }]
        });
      else {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + infiniteLimit + '&name=' + $scope.findByName.search;
        Main.requestApi(accessToken, urlApi, successRequestName, $scope.errorRequest);

      }
    }

    $scope.findByNpk = {
      search: ""
    };
    $scope.searchByNpk = function (page) {
      $scope.findStatus = false;
      if ($scope.findByNpk.search == null || $scope.findByNpk.search == '')
        $ionicPopup.show({
          title: "Text field can't be empty!",
          buttons: [{
            text: 'OK'
          }]
        });
      else {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + size + '&npk=' + $scope.findByNpk.search;
        Main.requestApi(accessToken, urlApi, successRequestNpk, $scope.errorRequest);

      }
    }

    $scope.findByGroupCode = {
      search: ""
    };
    $scope.searchByGroupCode = function (index) {
      $scope.findStatus = false;
      if ($scope.findByGroupCode.search == null || $scope.findByGroupCode.search == '') {
        $ionicPopup.show({
          title: "Text field can't be empty!",
          buttons: [{
            text: 'OK'
          }]
        });
      } else {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?code=' + $scope.findByGroupCode.search;
        // var urlApi = 'http://192.168.1.101:7878/api/attendanceGroup/search?code=' + $scope.findByGroupCode.search;
        Main.requestApi(accessToken, urlApi, successRequestGroupCode, $scope.errorRequest);

      }
    }

    $scope.findByGroupName = {
      search: ""
    };
    $scope.searchByGroupName = function (index) {
      $scope.findStatus = false;
      if ($scope.findByGroupName.search == null || $scope.findByGroupName.search == '') {
        $ionicPopup.show({
          title: "Text field can't be empty!",
          buttons: [{
            text: 'OK'
          }]
        });
      } else {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/attendanceGroup/search?name=' + $scope.findByGroupName.search;
        // var urlApi = 'http://192.168.1.101:7878/api/attendanceGroup/search?code=' + $scope.findByGroupCode.search;
        Main.requestApi(accessToken, urlApi, successRequestGroupName, $scope.errorRequest);

      }
    }

    $scope.doRefresh = function (index) {
      // $scope.noDataShow = false;

      if (index == 1) {
        $scope.isLoadMoreShow = false;
        $scope.npkItems = [];
      } else if (index == 2) {
        j = 0;
        $scope.isLoadMoreShow = false;
        $scope.nameItems = [];

      } else if (index == 33) {
        $scope.isLoadMoreShow = false;
        $scope.groupCodeItems = [];
      } else if (index == 44) {
        $scope.isLoadMoreShow = false;
        $scope.groupNameItems = [];
      }
    }

    // $scope.getValue = function(name,npk,index){
    //     $scope.requestHeader.empName = name;
    //     $scope.requestHeader.npk = npk;
    //     if (index == 1)
    //       $scope.modal1.hide();
    //     else
    //       $scope.modal2.hide();
    // }



    $scope.getValue = function (name, npk, employeeId, index) {
      $scope.requestHeader.empName = name;
      $scope.requestHeader.npk = npk;

      // alert(employeeId);
      // $scope.requestHeader.employeeId = employeeId;

      // setter employeeId
      Main.saveDataEmployeeId(employeeId);

      if (index == 1)
        $scope.modal1.hide();
      else
        $scope.modal2.hide();
    }


    $scope.getValueByGroup = function (groupCode, groupName, index) {
      $scope.requestHeader.groupCode = groupCode;
      $scope.requestHeader.groupName = groupName;
      if (index == 33) $scope.modalByGroupCode.hide();
      if (index == 44) $scope.modalByGroupName.hide();
    }

    $ionicModal.fromTemplateUrl('modal1.html', {
      id: '1',
      scope: $scope
    }).then(function (modal) {
      $scope.modal1 = modal;
    });
    $ionicModal.fromTemplateUrl('modal2.html', {
      id: '2',
      scope: $scope
    }).then(function (modal) {
      $scope.modal2 = modal;
    });
    $ionicModal.fromTemplateUrl('modalByEmployee.html', {
      id: '11',
      scope: $scope
    }).then(function (modal) {
      $scope.modalByEmployee = modal;
    });
    $ionicModal.fromTemplateUrl('modalByGroup.html', {
      id: '22',
      scope: $scope
    }).then(function (modal) {
      $scope.modalByGroup = modal;
    });
    $ionicModal.fromTemplateUrl('modalByGroupCode.html', {
      id: '33',
      scope: $scope
    }).then(function (modal) {
      $scope.modalByGroupCode = modal;
    });
    $ionicModal.fromTemplateUrl('modalByGroupName.html', {
      id: '44',
      scope: $scope
    }).then(function (modal) {
      $scope.modalByGroupName = modal;
    });

    $scope.openModal = function (index) {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      if (index == 1) $scope.modal1.show();
      else if (index == 2) $scope.modal2.show();
      else if (index == 11) $scope.modalByEmployee.show();
      else if (index == 22) $scope.modalByGroup.show();
      else if (index == 33) $scope.modalByGroupCode.show();
      else if (index == 44) $scope.modalByGroupName.show();


    };

    $scope.closeModal = function (index) {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      if (index == 1) $scope.modal1.hide();
      else if (index == 2) $scope.modal2.hide();
      else if (index == 11) $scope.modalByEmployee.hide();
      else if (index == 22) $scope.modalByGroup.hide();
      else if (index == 33) $scope.modalByGroupCode.hide();
      else if (index == 44) $scope.modalByGroupName.hide();
    };

    var successRequestNpk = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();
      $scope.nameItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.npkItems.push(obj);


        if ($scope.npkItems.length >= infiniteLimit || $scope.npkItems.length <= res.numberOfElements && $scope.npkItems.length != 1) {

          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.npkItems.length == 0 || $scope.npkItems.length == 1) {


          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }
    }

    var successRequestGroupCode = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $scope.groupCodeItems = [];
      $ionicLoading.hide();
      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.groupCodeItems.push(obj);

        if ($scope.groupCodeItems.length >= infiniteLimit || $scope.groupCodeItems.length <= res.numberOfElements) {
          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.groupCodeItems.length == 0) {
          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }
    }

    var successRequestGroupName = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $scope.groupNameItems = [];
      $ionicLoading.hide();
      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.groupNameItems.push(obj);

        if ($scope.groupNameItems.length >= infiniteLimit || $scope.groupNameItems.length <= res.numberOfElements) {
          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.groupNameItems.length == 0) {
          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }
    }

    var successRequestName = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();

      $scope.nameItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.nameItems.push(obj);

        if ($scope.nameItems.length >= infiniteLimit || $scope.nameItems.length <= res.numberOfElements) {
          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.nameItems.length == 0) {
          $scope.isLoadMoreShow = false;
        }

        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }



    }

    var successRequestCount = function (res) {
      if (res != null) {
        $rootScope.countApproval = res.count;
        $scope.general.countApproval = res.count;
      }
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

    function initModule() {
      $scope.profile = Main.getSession("profile");
      $scope.findStatus = false;
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      // getNeedApproval('benefit',0);
      // getCountNeedApproval();
      // $scope.loadMoreName();

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }

  })


  .controller('AddAssignAttendancegroupCtrl', 
    function (
      $stateParams, 
      ionicDatePicker, 
      $filter, 
      $ionicLoading, 
      $scope, 
      $state, 
      Main
    ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    $scope.module = {};
    $scope.module.type = 'general';
    $scope.attendanceGroup = {};
    $scope.detailattendanceGroup = {};
    $scope.listdetailattendanceGroup = {};


    //default end time

    // $scope.attendanceGroup.effectiveEndDate = '4000-12-31';


    var attendanceGroupId = $stateParams.id;


    $scope.attendanceGroupUpdate = {};


    // jika stateparams update
    if ($stateParams.id != null || $stateParams.id > 0) {
      $scope.attendanceGroup.id = $stateParams.id;
    }


    $scope.populateData = {};

    var npk = $stateParams.npk;
    var npkStorage = Main.getDataEmployeeNpk();
    var empName = $stateParams.empName;

    $scope.employeeName = Main.getDataEmployee();
    $scope.attendanceGroup.employeeId = Main.getDataEmployeeId();


    $scope.defaultId = $stateParams.id;



    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    $scope.goToDetailattendancegroup = function (id, patternId) {
      $state.go('app.detailattendancegroup', {
        'id': id,
        'patternId': patternId
      });
    }


    $scope.goToAddDetailAd = function () {
      $state.go('app.addattendancegroupdetail', {
        'id': attendanceGroupId
      });
    }


    var datepicker3 = {
      callback: function (val) { //Mandatory

        $scope.attendanceGroup.effectiveStartDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker1 = function () {

      ionicDatePicker.openDatePicker(datepicker3);
    };


    var datepicker4 = {
      callback: function (val) { //Mandatory


        $scope.attendanceGroup.effectiveEndDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker2 = function () {

      ionicDatePicker.openDatePicker(datepicker4);
    };


    function getDetailattendanceGroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/empGroup/' + attendanceGroupId;


      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);
    }


    var successattendanceGroup = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.attendanceGroup.employeeId = res.employeeId;
        $scope.attendanceGroup.groupId = res.group.id;
        $scope.attendanceGroup.effectiveStartDate = res.effectiveStartDate;
        $scope.attendanceGroup.effectiveEndDate = res.effectiveEndDate;
        $scope.attendanceGroup.remark = res.remark;




        $scope.populateData = {
          id: res.group.id
        };
      }
    }


    $scope.getData = function (data) {
      $scope.attendanceGroup.groupId = data.id;
    }

    function getDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + attendanceGroupId;


      Main.requestApi(accessToken, urlApi, successattendanceGroupDetail, $scope.errorRequest);
    }


    var successattendanceGroupDetail = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.detailattendanceGroup = res;
      }
    }


    function getListDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup/' + attendanceGroupId + '/detail';


      Main.requestApi(accessToken, urlApi, successattendanceGroupListDetail, $scope.errorRequest);
    }


    var successattendanceGroupListDetail = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.listdetailattendanceGroup = res;
      }
    }


    $scope.indentityPage = $stateParams.id;

    $scope.saveData = function (id) {


      // var startDate = new Date($scope.attendanceGroup.effectiveStartDate);
      var startDate = $filter('date')(new Date($scope.attendanceGroup.effectiveStartDate), 'yyyy-MM-dd');
      var endDate = $filter('date')(new Date($scope.attendanceGroup.effectiveEndDate), 'yyyy-MM-dd');


      // var endDate = new Date($scope.attendanceGroup.effectiveEndDate);



      if (endDate <= startDate) {
        $scope.warningAlert("Efective Start Date Must Be Lower Than Efective End Date");
      } else {
        saveDataattendanceGroup(id);
      }



    }

    function verificationForm(attendanceGroup) {

      if (attendanceGroup.groupId == undefined || attendanceGroup.groupId == '') {
        messageValidation = "Attandance Group can't be empty";
        return false;
      } else if (attendanceGroup.effectiveStartDate == undefined || attendanceGroup.effectiveStartDate == 'Invalid Date') {


        messageValidation = "Effective Start Date From Pattern can't be empty";
        return false;
      } else if (attendanceGroup.effectiveEndDate == undefined || attendanceGroup.effectiveEndDate == 'Invalid Date') {
        messageValidation = "Effective End Date can't be empty";
        return false;
      }

      return true;
    }

    function saveDataattendanceGroup(id) {

      $scope.attendanceGroup.effectiveStartDate = $filter('date')(new Date($scope.attendanceGroup.effectiveStartDate), 'yyyy-MM-dd');
      $scope.attendanceGroup.effectiveEndDate = $filter('date')(new Date($scope.attendanceGroup.effectiveEndDate), 'yyyy-MM-dd');

      if (verificationForm($scope.attendanceGroup)) {
        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.attendanceGroup);



        var urlApi;


        if (id > 0) {
          var urlApi = Main.getUrlApi() + '/api/empGroup/' + id;
          Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
        } else {
          var urlApi = Main.getUrlApi() + '/api/empGroup';
          Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

        }
      } else {
        $scope.warningAlert(messageValidation);
      }




    }


    function getListAttandGroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/attendanceGroup';


      Main.requestApi(accessToken, urlApi, successAttendanceGroup, $scope.errorRequest);
    }


    var successAttendanceGroup = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.attendancegroupList = res.content;
      }
    }


    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);

      // $rootScope.refreshAssignattendanceGroupCtrl = true;
      // $scope.goBack('app.assignattendancegroup');


      $state.go('app.assignattendancegroup', {
        'npk': npk
      });

      // $state.go('app.assignattendancegroup', { 'npk': 13483, 'empName' : 'LIDYA JOHAN'});
      // $route.reload();

    }


    function initModule() {
      if (attendanceGroupId > 0) {
        getDetailattendanceGroup();
        getListDetail();
      }
      getListAttandGroup();
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });


  })

  .controller('DetailAssignAttendancegroupCtrl', 
    function (
      $stateParams,
      ionicDatePicker, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main
    ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }



    $scope.module = {};
    $scope.detailattendanceGroup = {};
    $scope.attendanceGroup = {};
    $scope.populateData = {};
    $scope.populateDataPatternDetail = {};
    $scope.attendanceGroupCode = {};
    $scope.shiftList = {};
    $scope.patternList = {};
    $scope.attendanceGroupName = {};
    var attendanceGroupId = $stateParams.id;
    $scope.defaultId = $stateParams.id;
    $scope.defaultPatternId = $stateParams.patternId;
    var dataPatternId = 0;
    $scope.patternDetail = 0;
    $scope.patternIdGlobal = 0;
    $scope.patternIdGlobalDetail = {};
    $scope.isShowAfterSelect = 0;
    $scope.startformPatternDetail = 0;
    $scope.startformPatternDetailData = 0;

    $scope.chooseTab = function (data) {
      $scope.module.type = data;
    }


    function getDetailattendanceGroup() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;

      var urlApi = Main.getUrlApi() + '/api/attendanceGroupDetail/' + attendanceGroupId;
      Main.requestApi(accessToken, urlApi, successattendanceGroup, $scope.errorRequest);


      getDetailpattern();
    }


    var successattendanceGroup = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.detailattendanceGroup = res;
        $scope.attendanceGroupName = res.group.groupName;
        $scope.populateData = {
          id: res.patternDtl.patternId
        };
        $scope.patternIdGlobal = res.patternDtl.patternId;
        $scope.startformPatternDetailData = res.patternDtl.id;



        $scope.populateDataPatternDetail = {
          id: res.patternDtl.id
        };
      }
    }


    function getPattern() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern';
      Main.requestApi(accessToken, urlApi, successpattern, $scope.errorRequest);
    }


    var successpattern = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.patternList = res;
      }
    }



    function getListShift() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/shift';


      Main.requestApi(accessToken, urlApi, successShift, $scope.errorRequest);
    }


    var successShift = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.shiftList = res;
      }
    }


    $scope.getData = function (data) {
      $scope.detailattendanceGroup.patternId = data.id;
      $scope.patternIdGlobal = data.id;
      $scope.isShowAfterSelect = 1;
      getDetailpattern();



    }


    $scope.getDataDetail = function (data) {
      $scope.detailattendanceGroup.patternDtlId = data.id;
    }


    function getDetailpattern() {


      var dataPatternId = $scope.patternIdGlobal;

      if (dataPatternId == null || dataPatternId == '' || dataPatternId == 0) {

        var dataPatternId = $scope.defaultPatternId;
      }


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/pattern/' + dataPatternId + '/detail';


      Main.requestApi(accessToken, urlApi, successpatternDetailData, $scope.errorRequest);
    }


    var successpatternDetailData = function (res) {
      if (res != null) {
        $ionicLoading.hide();
        $scope.patternDetail = res;
      }
    }


    $scope.saveData = function (id) {
      saveDataattendanceGroup(id);
    }



    function saveDataattendanceGroup(id) {
      $ionicLoading.show({
        template: 'Submit Request...'
      });

      var accessToken = Main.getSession("token").access_token;
      var data = JSON.stringify($scope.detailattendanceGroup);




      var urlApi = Main.getUrlApi() + '/api/attendanceGroupDetail/' + id;
      Main.putRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);


    }

    var datepicker1 = {
      callback: function (val) { //Mandatory

        $scope.detailattendanceGroup.effectiveStartDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };



    var datepicker2 = {
      callback: function (val) { //Mandatory

        $scope.detailattendanceGroup.effectiveEndDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker2 = function () {
      ionicDatePicker.openDatePicker(datepicker2);
    };




    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAttendancegroupCtrl = true;
      $scope.goBack('app.attendancegroup');


    }


    function initModule() {
      getDetailattendanceGroup();
      getListShift();
      getPattern();
      getDetailpattern();
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })
  .controller("WorkLocationCtrl", function (
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var size = Main.getDataDisplaySize();
    var page = 0;
    $scope.requests = [];
    $scope.isLast = false;

    $scope.$on("$ionicView.beforeEnter", function () {
      initMethod();
    });


    $scope.loadMore = function () {
      page++;
      getMoreWorkLocation(page);

    };

    $scope.gotoDetail = function (id) {
      $state.go("app.worklocationdetail", {
        id: id
      });
    };

    $scope.refresh = function () {
      page = 0;
      $scope.requests = [];
      $scope.data.searchData = [];
      getMoreWorkLocation(0);
    };

    function initMethod() {
      $scope.requests = [];
      $scope.data.searchData = [];
      page = 0;
      getMoreWorkLocation(0);

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }

    function getMoreWorkLocation(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      if ($scope.data.searchData == '') {
        var urlApi =
          Main.getUrlApi() + "/api/worklocation?page=" + page + "&size=" + size;
        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      } else {
        var urlApi =
          Main.getUrlApi() + "/api/worklocation/search?page=" + page + "&size=" + size + "&name=" + $scope.data.searchData;
        Main.requestApi(accessToken, urlApi, sucessSearch, $scope.errorRequest);
      }


    }

    function searchData(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/worklocation/search?page=" + page + "&size=" + size + "&name=" + $scope.data.searchData;
      Main.requestApi(accessToken, urlApi, sucessSearch, $scope.errorRequest);
    }

    var sucessSearch = function (res) {
      $scope.requests = res.content;
      $scope.isLast = res.last;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };

    var successRequest = function (res) {
      $scope.requests = $scope.requests.concat(res.content);
      $scope.isLast = res.last;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };

    $scope.search = function () {
      searchData(0);
    }


  })
  .controller("WorkLocationDetailCtrl", function (
    ionicSuperPopup,
    $ionicHistory,
    $stateParams,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }



    $scope.$on("$ionicView.beforeEnter", function () {
      initMethod();
    });

    function initMethod() {
      getDetail($stateParams.id);

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }

    function getDetail(id) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/worklocation/" + id;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    var successRequest = function (res) {
      $scope.data = res;

      $scope.populateData = {
        value: res.timezone
      };
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");

    };
    $scope.employeeId = Main.getSession("profile").employeeTransient.id;
    //var id = $stateParams.id;

    $scope.timezoneList = [{
      value: 7,
      label: '+7'
    }, {
      value: 8,
      label: '+8'
    }, {
      value: 9,
      'label': '+9'
    }];

    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.statusApproval = null;
    $scope.image = "img/placeholder.png";
    $scope.npwp = {};
    $scope.npwp.images = [];
    $scope.npwp.imagesData = [];
    $scope.imageData;
    var messageValidation = "";

    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var sucessSubmitData = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.worklocation");
    };

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token;
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500) $scope.errorAlert(err.message);
        else $scope.errorAlert("Please Check your connection");
      }
    };


    $scope.getData = function (data) {
      $scope.data.timezone = data.value;
    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    };

    var errRefreshToken = function (err, status) {};

    // function validationForm(selected){
    //     if(selected == undefined || selected == ""){
    //         messageValidation = "You must pick your new marital status!";
    //         return false;
    //     }

    //     if(selected == $scope.currentStatus) {
    //         messageValidation = "You can't pick the same marital status!";
    //         return false;
    //     }
    //     return true;
    // }

    function sendData() {
      $ionicLoading.show({
        template: "Update WorkLocation..."
      });
      var accessToken = Main.getSession("token").access_token;

      // must change endpoint url to be change npwp personal biodata
      var urlApi = Main.getUrlApi() + "/api/worklocation/" + $stateParams.id;

      var data = JSON.stringify($scope.data);
      Main.putRequestApi(
        accessToken,
        urlApi,
        data,
        sucessSubmitData,
        errorRequest
      );
    }

    $scope.submit = function () {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure the data submitted is correct ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendData();
          }
        }
      );
      // }else {
      //     $scope.warningAlert(messageValidation);
      // }
    };
  });
