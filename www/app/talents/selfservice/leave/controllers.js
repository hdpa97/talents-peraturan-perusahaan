angular.module('leave.controllers', [])
  .controller('DetailEmpDailyCtrl',
    function (
      $filter,
      ionicTimePicker,
      $timeout,
      $stateParams,
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      Main
    ) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      var empdailyIdx = $stateParams.idx;
      $scope.atempdaily = {};
      if (empdailyIdx != null)
        $scope.atempdaily = $rootScope.atempdaily[empdailyIdx];

      $scope.categories = [];
      $scope.leave = {};
      var messageValidation = "";
      $scope.attendance = {};
      $scope.attendanceIn = $filter('date')(new Date($scope.atempdaily.editInTime), 'HH:mm');
      $scope.attendanceOut = $filter('date')(new Date($scope.atempdaily.editOutTime), 'HH:mm');
      var employee = Main.getSession("profile").employeeTransient.id;

      $scope.onSelectType = function () {
        setLabelType($scope.attendance.type, getListType("Attendance Edit"));

      };

      function setLabelType(type, arrType) {
        if (arrType.length != 0) {
          for (var i = 0; i < arrType.length; i++) {
            if (arrType[i].type == type)
              $scope.labelType = arrType[i].typeLabel;
          };
        }
      }

      function getCategory(categoryType) {
        var sessTmRequestType = Main.getSession("tmCategoryType");

        for (var i = 0; i < sessTmRequestType.length; i++) {
          if (sessTmRequestType[i].categoryType == categoryType) {
            return sessTmRequestType[i];
          }

        }
        return undefined;
      }

      function getListType(categoryType) {

        var objCategory = getCategory(categoryType);
        if (objCategory != undefined)
          return objCategory.listRequestType;
        else
          return [];

      }

      function initData() {
        $scope.selectType = getListType("Attendance Edit");
        $scope.labelType = "Select Type ..";
      }

      initData();

      $scope.openTimePicker1 = function () {
        ionicTimePicker.openTimePicker(timePickerComponent1);
      };

      var timePickerComponent1 = {
        callback: function (val) {
          if (typeof (val) === 'undefined') {

          } else {
            var hours = parseInt(val / 3600);
            var minutes = (val / 60) % 60;
            var hourString = "" + hours;
            var minuteString = "" + minutes;

            if (hourString.length == 1)
              hourString = "0" + hours;

            if (minuteString.length == 1)
              minuteString = "0" + minutes;

            $scope.attendanceIn = hourString + ":" + minuteString;
          }
        },
        inputTime: 32400,
        format: 24,
        step: 1,
        setLabel: 'Set'
      };

      $scope.openTimePicker2 = function () {
        ionicTimePicker.openTimePicker(timePickerComponent2);
      };

      var timePickerComponent2 = {
        callback: function (val) {
          if (typeof (val) === 'undefined') {

          } else {
            var hours = parseInt(val / 3600);
            var minutes = (val / 60) % 60;
            var hourString = "" + hours;
            var minuteString = "" + minutes;

            if (hourString.length == 1)
              hourString = "0" + hours;

            if (minuteString.length == 1)
              minuteString = "0" + minutes;

            $scope.attendanceOut = hourString + ":" + minuteString;
          }
        },
        inputTime: 32400,
        format: 24,
        step: 1,
        setLabel: 'Set'
      };

      var successRequest = function (res) {
        $timeout(function () {
          $rootScope.data.requestLeaveVerification = res;
          $ionicLoading.hide();
          $state.go("app.leaveconfirmation");
        }, 1000);
      };


      $scope.submitForm = function () {
        if (verificationForm($scope.attendance)) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });

          $scope.atempdaily.editInTime = $filter('date')(new Date($scope.atempdaily.editInTime), 'yyyy-MM-dd');
          $scope.atempdaily.editOutTime = $filter('date')(new Date($scope.atempdaily.editOutTime), 'yyyy-MM-dd');
          var dataPost = {};
          dataPost.startDate = $scope.atempdaily.workDate;
          dataPost.endDate = $scope.atempdaily.workDate;
          dataPost.remark = $scope.attendance.remark;
          dataPost.typeDesc = $scope.labelType;
          dataPost.attendanceInTime = dataPost.startDate + " " + $scope.attendanceIn + ":00";
          dataPost.attendanceOutTime = dataPost.endDate + " " + $scope.attendanceOut + ":00";
          dataPost.categoryType = "Attendance Edit";
          dataPost.type = $scope.attendance.type;
          dataPost.workflow = "SUBMITAT";
          dataPost.module = "Time Management";
          dataPost.employee = employee;
          dataPost.atempdaily = $scope.atempdaily.id;

          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/user/tmrequestheader/verificationleave';
          var data = JSON.stringify(dataPost);
          Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);

        } else {
          $scope.warningAlert(messageValidation);
        }
      };

      function verificationForm(attendance) {
        if (attendance.type == undefined || attendance.type == "") {
          messageValidation = "Reason can't be empty";
          return false;
        }

        // if(attendance.remark == undefined || attendance.remark == ""){
        //     messageValidation = "Remark can't be empty";
        //     return false;
        // }
        return true;
      }

    })


  .controller('ListEmpDailyCtrl', function ($filter, $timeout, $ionicHistory, $ionicLoading, $rootScope, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    $scope.entryPage = true;
    $scope.year = [];
    $scope.month = Main.getSelectMonth();
    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refYear = Main.getDataReference(arrCompanyRef, 'payslip', null, 'selectYear');
    $scope.selected = {};
    $scope.today = new Date();
    $scope.categories = [];

    var size = Main.getDataDisplaySize();
    $scope.atempdaily = [];
    $scope.show = function () {
      $scope.entryPage = false;
      if ($scope.selected.year == '' || $scope.selected.month == '') {
        $scope.warningAlert("Please choose Month and Yeare");
        return false;
      } else {
        initMethod();
      }

    }

    $scope.goToAddTimesheet = function () {
      $state.go('app.addtimesheet');
      $ionicHistory.nextViewOptions({
        disableAnimate: false,
        disableBack: false
      });
    }

    $scope.goToDetails = function (idx) {
      $state.go('app.detailempdaily', {
        'idx': idx
      });
    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.atempdaily = res;

      $rootScope.atempdaily = [];
      for (var i = 0; i < $scope.atempdaily.length; i++) {
        var obj = $scope.atempdaily[i];
        obj.idx = i;
        $rootScope.atempdaily.push(obj);
      }
      $scope.atempdaily = $rootScope.atempdaily;
      $scope.$broadcast('scroll.refreshComplete');
      $rootScope.refreshListTimesheetCtrl = true;
    }

    function initMethod() {
      $scope.atempdaily = [];
      getAtEmpDaily();
    }



    $scope.refresh = function () {

      $scope.atempdaily = [];
      getAtEmpDaily();
    }

    var successRequestss = function (res) {
      $timeout(function () {
        if (res.length > 0) {
          Main.setSession("tmCategoryType", res);
          $scope.categories = res;

        }
        $ionicLoading.hide();
      }, 1000);
    }


    function getListCategory() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequest/category?module=time management';
      //var urlApi = Main.getUrlApi() + '/api/user/tmrequest/category?module=attendance';
      Main.requestApi(accessToken, urlApi, successRequestss, $scope.errorRequest);
    }

    function initModule() {
      $scope.selected.year = "";
      $scope.selected.month = "";
      $scope.entryPage = false;
      if (Main.getSession("tmCategoryType") == undefined)
        getListCategory();
      else
        $scope.categories = Main.getSession("tmCategoryType");

      // if( $rootScope.refreshListTimesheetCtrl) {
      //     initMethod();
      // }

      if (refYear != undefined && refYear != '') {
        $scope.year = JSON.parse(refYear);
      }

      $rootScope.refreshListTimesheetCtrl = false;

    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initModule();

    })


    function getAtEmpDaily() {

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var month = "";
      if ($scope.selected.month != null || $scope.selected.year != null) {
        var bulan = $scope.selected.month;
        var year = $scope.selected.year;
        var month = year + '-' + bulan;
      } else {
        var month = $filter('date')(new Date(), 'yyyy-MM');
      }

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/atempdaily/byperiod?type=monthly&month=' + month;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

  })

  .controller('HomeLeaveCtrl', function ($ionicLoading, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.leaves = [];
    $scope.total = {};
    $scope.refresh = function () {
      initMethod();
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.leaves = res;
      $scope.$broadcast('scroll.refreshComplete');

    }

    function getTotalFromArray(array, module, category) {
      for (var i = array.length - 1; i >= 0; i--) {
        if (array[i].module.toLowerCase() == module && array[i].category.toLowerCase() == category)
          return array[i].total;
      };
      return "-";
    }
    var successTotalCategory = function (res) {

      if (res != undefined && res.length > 0) {
        $scope.total.sick = getTotalFromArray(res, "time management", "sick");
        $scope.total.leave = getTotalFromArray(res, "time management", "leave");
        $scope.total.permission = getTotalFromArray(res, "time management", "permission");
      }
    }


    function getLeaves() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequest/bystatus?module=Time Management&status=pending';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function getTotalCategory() {
      $scope.total.permission = "-";
      $scope.total.leave = "-";
      $scope.total.sick = "-";
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequest/totalamount?fromdate=2017-01-01&todate=2017-31-12';
      Main.requestApi(accessToken, urlApi, successTotalCategory, $scope.errorRequest);
    }

    function initModule() {
      getLeaves();
      getTotalCategory();
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();

    });



  })

  .controller('ChooseLeaveCategoryCtrl', function ($timeout, $ionicLoading, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.categories = [];
    // $scope.isFeatureTester = Main.getSession("profile").isFeatureTester;
    $scope.gradeNominal = Main.getSession("profile").employeeTransient.assignment.gradeNominal;

    $scope.goToLeaveForm = function (category) {

      if (category == 'Attendance Edit') {
        $state.go("app.listempdaily");
      } else {
        $state.go("app.addleave", {
          'category': category
        });
      }

    }

    var successRequest = function (res) {


      $timeout(function () {
        if (res.length > 0) {
          Main.setSession("tmCategoryType", res);
          $scope.categories = res;
        }
        $ionicLoading.hide();
      }, 1000);

    }

    // $scope.hideCheckInOut = function(){
    //     if (res.id == 23)
    //         return true;
    // }

    function getListCategory() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequest/category?module=time management';
      //var urlApi = Main.getUrlApi() + '/api/user/tmrequest/category?module=attendance';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function initModule() {

      if (Main.getSession("tmCategoryType") == undefined)
        getListCategory();
      else
        $scope.categories = Main.getSession("tmCategoryType");

    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })

  .controller('LeaveConfirmationCtrl', function (appService, $ionicActionSheet, $cordovaCamera, $filter, $ionicLoading, $rootScope, $scope, Main) {
    var leaveVerification = $rootScope.data.requestLeaveVerification;
    $scope.defaultImage = "img/placeholder.png";
    $scope.images = [];
    $scope.imagesData = [];
    $scope.requestHeader = {};
    $scope.requestHeader.attachments = [];
    $scope.appMode = Main.getAppMode();
    $scope.buttonDisable = false;

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initMethod();
      $scope.buttonDisable = false;
    });

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.goTo("app.selfservicesuccess");
    }

    $scope.removeChoice = function () {
      var lastItem = $scope.requestHeader.attachments.length - 1;
      $scope.requestHeader.attachments.splice(lastItem);
      $scope.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.images.length > 2) {
        alert("Only 3 pictures can be upload");
        return false;
      }
      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.imagesData.push({
                    'image': imageData
                  });
                  $scope.requestHeader.attachments.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.imagesData.push({
                    'image': imageData
                  });
                  $scope.requestHeader.attachments.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };


    $scope.submitForm = function () {
      if ($scope.requestType.requiredAttachment && $scope.requestHeader.attachments.length == 0) {
        $scope.warningAlert("You must add at least 1 attachment.");
        return false;
      }

      $scope.buttonDisable = true;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequestheader/leave';
      var attachment = [];

      if ($scope.requestHeader.attachments.length > 0) {
        for (var i = $scope.requestHeader.attachments.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.requestHeader.attachments[i].image
            };
          } else {
            if ($scope.requestHeader.attachments[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.requestHeader.attachments[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);
        };
      }

      leaveVerification = $rootScope.data.requestLeaveVerification;
      leaveVerification.startDate = $filter('date')(new Date(leaveVerification.startDate), 'yyyy-MM-dd');
      leaveVerification.endDate = $filter('date')(new Date(leaveVerification.endDate), 'yyyy-MM-dd');

      if (leaveVerification.attendanceInTime != undefined && leaveVerification.attendanceOutTime != undefined) {
        leaveVerification.attendanceInTime = $filter('date')(new Date(leaveVerification.attendanceInTime), 'yyyy-MM-dd HH:mm:dd');
        leaveVerification.attendanceOutTime = $filter('date')(new Date(leaveVerification.attendanceOutTime), 'yyyy-MM-dd HH:mm:dd');
      }

      if (leaveVerification.startDateInTime != undefined && leaveVerification.endDateInTime != undefined) {
        leaveVerification.startDateInTime = $filter('date')(new Date(leaveVerification.startDateInTime), 'yyyy-MM-dd HH:mm:dd');
        leaveVerification.endDateInTime = $filter('date')(new Date(leaveVerification.endDateInTime), 'yyyy-MM-dd HH:mm:dd');
      }

      if (leaveVerification.startDateOutTime != undefined && leaveVerification.endDateOutTime != undefined) {
        leaveVerification.startDateOutTime = $filter('date')(new Date(leaveVerification.startDateOutTime), 'yyyy-MM-dd HH:mm:dd');
        leaveVerification.endDateOutTime = $filter('date')(new Date(leaveVerification.endDateOutTime), 'yyyy-MM-dd HH:mm:dd');
      }

      leaveVerification.attachments = attachment;

      var data = JSON.stringify(leaveVerification);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);

    }

    function initData() {
      $scope.total = 0;
      $scope.totalBalance = 0;
      $scope.type = "-";
      $scope.categoryType = "-";
      $scope.images = [];
      $scope.imagesData = [];
      $scope.requestHeader = {};
      $scope.requestHeader.attachments = [];
      $scope.overtimeIn = 0;
      $scope.overtimeOut = 0;
      $scope.typeDesc = "-";
      $scope.attendanceInTime = 0;
      $scope.attendanceOutTime = 0;
      $scope.requestType = {};
    }

    function initMethod() {
      initData();
      leaveVerification = $rootScope.data.requestLeaveVerification;
      if (leaveVerification != null) {
        $scope.categoryType = leaveVerification.categoryType.toLowerCase();
        $scope.total = leaveVerification.total;
        $scope.type = leaveVerification.type;
        $scope.startDate = leaveVerification.startDate;
        $scope.endDate = leaveVerification.endDate;
        $scope.totalBalance = leaveVerification.totalBalance;
        $scope.overtimeIn = leaveVerification.overtimeIn;
        $scope.overtimeOut = leaveVerification.overtimeOut;
        $scope.attendanceInTime = leaveVerification.attendanceInTime;
        $scope.attendanceOutTime = leaveVerification.attendanceOutTime;
        $scope.typeDesc = leaveVerification.typeDesc;
        $scope.requestType = leaveVerification.requestType;
        $scope.startDateInTime = leaveVerification.startDateInTime;
        $scope.endDateInTime = leaveVerification.endDateInTime;
        $scope.startDateOutTime = leaveVerification.startDateOutTime;
        $scope.endDateOutTime = leaveVerification.endDateOutTime;
        $scope.atempdaily = leaveVerification.atempdaily;


      }
    }


  })


  .controller('AddLeaveCtrl', function (ionicTimePicker, $timeout, $filter, $stateParams, $ionicLoading, $rootScope, $scope, $state, Main, ionicDatePicker) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    var startDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    var endDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    var employee = Main.getSession("profile").employeeTransient.id;
    $scope.leaveCategory = $stateParams.category.toLowerCase();
    $scope.categoryList = $stateParams.category;
    $scope.leaveType = {};
    $scope.selectHour = Main.getSelectHour();
    $scope.selectMinutes = Main.getSelectFiveteenMin();
    $scope.selectType = [];
    $scope.selectEmployeeSubstitute = [];
    $scope.labelSubstitute = "Select Substitute ..";
    $scope.labelType = "Select Type ..";
    $scope.leave = {};
    //$scope.leave = {remark:"",startDate:new Date(),endDate:new Date(),remark:"",type:""};
    $scope.balance = {
      balanceEnd: "-",
      balanceUsed: "-"
    };
    $scope.attendanceIn = "00:00";
    $scope.attendanceOut = "00:00";
    var module = "time management";
    var messageValidation = "";





    var date = new Date();

    $scope.selectMonth = Main.getSelectMonth();
    $scope.selectYear = [];
    $scope.search = {};
    $scope.absenceList = {};
    $scope.currentYear = $filter("date")(new Date(), "yyyy");
    $scope.currentMonth = $filter("date")(new Date(), "MMM").toUpperCase();

    // commented by JUW, no need to use this variable anymore.
    // $scope.dataMonth =
    //   Main.getIdfromValue(
    //     $scope.selectMonth,
    //     $scope.currentMonth
    //   );

    $scope.minusYear = $scope.currentYear - 1;

    $scope.search.month = $scope.currentMonth;
    $scope.search.year = $scope.currentYear;

    $scope.selectYear.push($scope.currentYear);
    $scope.selectYear.push($scope.minusYear);


    $scope.goToOvertime = function (category) {


      $state.go("app.addovertime", {
        'category': category
      });


    }

    $scope.goToRealization = function (category) {
      $state.go("app.overtimerealization", {
        'category': category
      });
    }

    //Overtime Component
    var timePickerOvertime1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.leave.startDateInTimeTime = hourString + ":" + minuteString;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOvertime1 = function () {
      ionicTimePicker.openTimePicker(timePickerOvertime1);
    };


    var timePickerOvertime2 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.leave.endDateInTimeTime = hourString + ":" + minuteString;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOvertime2 = function () {
      ionicTimePicker.openTimePicker(timePickerOvertime2);
    };


    var timePickerOver1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.leave.startTime = hourString + ":" + minuteString;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver1 = function () {
      ionicTimePicker.openTimePicker(timePickerOver1);
    };



    var timePickerOver2 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.leave.endTime = hourString + ":" + minuteString;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 1, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver2 = function () {
      ionicTimePicker.openTimePicker(timePickerOver2);
    };


    var datepickerOvertime1 = {
      callback: function (val) { //Mandatory
        $scope.leave.startDateInTimeDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePickerOvertime1 = function () {
      ionicDatePicker.openDatePicker(datepickerOvertime1);
    };


    var datepickerOvertime2 = {
      callback: function (val) { //Mandatory
        $scope.leave.endDateInTimeDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePickerOvertime2 = function () {
      ionicDatePicker.openDatePicker(datepickerOvertime2);
    };



    $scope.openDatePickerOvertime4 = function () {
      ionicDatePicker.openDatePicker(datepickerOvertime4);
    };


    var datepickerOvertime4 = {
      callback: function (val) { //Mandatory
        $scope.leave.startTime = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "HH-mm",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };



    $scope.openDatePickerOvertime5 = function () {
      ionicDatePicker.openDatePicker(datepickerOvertime5);
    };


    var datepickerOvertime5 = {
      callback: function (val) { //Mandatory
        $scope.leave.endTime = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "HH-mm",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };


    var timePickerComponent1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.attendanceIn = hourString + ":" + minuteString;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 5, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePicker1 = function () {
      ionicTimePicker.openTimePicker(timePickerComponent1);
    };

    var timePickerComponent2 = {
      callback: function (val) { //Mandatory
        $scope.leave.attendanceOut = new Date(val * 1000);
        var hours = parseInt(val / 3600);
        var minutes = (val / 60) % 60;
        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.attendanceOut = hourString + ":" + minuteString;
        }
      },
      inputTime: 61200, //Optional
      format: 24, //Optional
      step: 5, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePicker2 = function () {
      ionicTimePicker.openTimePicker(timePickerComponent2);
    };




    $scope.onSelectType = function () {
      // $scope.labelType = $scope.leave.type;
      setLabelType($scope.leave.type, getListType($stateParams.category))
      getCurrentBalanceType(module, $scope.leaveCategory, $scope.leave.type);
    }

    function setLabelSubstitute(employmentId, arrEmployee) {
      if (arrEmployee.length != 0) {
        for (var i = 0; i < arrEmployee.length; i++) {
          if (arrEmployee[i].employment == employmentId)
            $scope.labelSubstitute = arrEmployee[i].fullName;
        };
      }
    }

    function setLabelType(type, arrType) {
      if (arrType.length != 0) {
        for (var i = 0; i < arrType.length; i++) {
          if (arrType[i].type == type)
            $scope.labelType = arrType[i].typeLabel;
        };
      }
    }



    $scope.onSelectSubstitute = function () {
      setLabelSubstitute($scope.leave.substituteToEmployment, $rootScope.selectEmployeeSubstitute);
    }



    var datepicker = {
      callback: function (val) { //Mandatory
        $scope.leave.startDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    var datepicker1 = {
      callback: function (val) { //Mandatory
        $scope.leave.endDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    var datepickerRealization = {
      callback: function (val) { //Mandatory
        $scope.leave.realizationDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };

    $scope.openDatePickerOverRealization = function () {
      ionicDatePicker.openDatePicker(datepickerRealization);
    };


    var successRequest = function (res) {
      $timeout(function () {
        $rootScope.data.requestLeaveVerification = res;
        $ionicLoading.hide();
        $state.go("app.leaveconfirmation");
      }, 1000);
    }

    var successGetBalance = function (res) {
      $ionicLoading.hide();
      $scope.balance = res;
      // var balanceEnd =0;
      // var balanceUsed=0;
      // for(var i=0;i<res.length;i++) {
      //     balanceEnd += res[i].balanceEnd;
      //     balanceUsed += res[i].balanceUsed;
      // }
      // $scope.balance.balanceEnd = balanceEnd;
      // $scope.balance.balanceUsed = balanceUsed;
    }
    var errorRequestBalance = function (res) {
      $ionicLoading.hide();
    }

    function getCurrentBalanceType(module, category, type) {
      $scope.balance = {
        balanceEnd: "-",
        balanceUsed: "-"
      };
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmbalance/currenttype?module=' + module + '&category=' + category + '&type=' + type;
      Main.requestApi(accessToken, urlApi, successGetBalance, errorRequestBalance);
    }

    $scope.onChangeType = function () {
      $scope.balance = {
        balanceEnd: "-",
        balanceUsed: "-"
      };
      if ($scope.leave.type != "") {
        getCurrentBalanceType(module, $scope.leaveCategory, $scope.leave.type);
      }

    }


    function diffTwoDateTimeMiliSecond(stringStartDate, stringEndDate) {
      var startDate = (new Date(stringStartDate + "Z")).getTime();
      var endDate = (new Date(stringEndDate + "Z")).getTime();

      return Math.floor(endDate - startDate);
    }

    function diffTwoDateMinutes(stringStartDate, stringEndDate) {
      var startDate = (new Date(stringStartDate + "Z")).getTime();

      var endDate = (new Date(stringEndDate + "Z")).getTime();

      var microSecondsDiff = Math.abs(endDate - startDate);

      // Number of milliseconds per day =
      //   24 hrs/day * 60 minutes/hour * 60 seconds/minute * 1000 msecs/second
      var minuteDiff = Math.floor(microSecondsDiff / (1000 * 60));


      return minuteDiff;

    }

    $scope.submitForm = function () {
      if ($scope.leaveCategory == 'overtime') {
        var startTimeOvertime = $scope.leave.startDateInTimeTime;
        var endTimeOvertime = $scope.leave.endDateInTimeTime;
        var startDateInTime = $filter('date')(new Date($scope.leave.startDateInTimeDate), 'yyyy-MM-dd') + " " + startTimeOvertime + ":00";
        var endDateInTime = $filter('date')(new Date($scope.leave.endDateInTimeDate), 'yyyy-MM-dd') + " " + endTimeOvertime + ":00";
        $scope.leave.startDateInTime = startDateInTime;
        $scope.leave.endDateInTime = endDateInTime;
      }


      //return false;
      if (verificationForm($scope.leave)) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var objCategory = getCategory($stateParams.category);

        $scope.leave.categoryType = $stateParams.category;
        $scope.leave.startDate = $filter('date')(new Date($scope.leave.startDate), 'yyyy-MM-dd');
        $scope.leave.endDate = $filter('date')(new Date($scope.leave.endDate), 'yyyy-MM-dd');
        var dataPost = {};
        if ($scope.leaveCategory == "attendance edit") {
          dataPost.startDate = $scope.leave.startDate;
          dataPost.endDate = dataPost.startDate;
          dataPost.remark = $scope.leave.remark;
          dataPost.typeDesc = $scope.labelType;
          //dataPost.type = "Edit Attendance";
          dataPost.attendanceInTime = dataPost.startDate + " " + $scope.attendanceIn + ":00";
          dataPost.attendanceOutTime = dataPost.startDate + " " + $scope.attendanceOut + ":00";

        } else if ($scope.leaveCategory == 'overtime') {
          dataPost.startDate = $filter('date')(new Date($scope.leave.startDateInTimeDate), 'yyyy-MM-dd'); //$scope.leave.startDate;
          dataPost.typeDesc = $scope.labelType;
          dataPost.endDate = $filter('date')(new Date($scope.leave.endDateInTimeDate), 'yyyy-MM-dd'); //dataPost.startDate;
          dataPost.remark = $scope.leave.remark;
          //var dateStartDateInTime = calcTime(new Date(startDateInTime),'Jakarta','+7');//new Date(startDateInTime);

          //return false;
          //dataPost.type = "Overtime";
          // dataPost.overtimeIn = (parseInt($scope.leave.overtimeinhour) * 60) + parseInt($scope.leave.overtimeinmin) ;
          // dataPost.overtimeOut = (parseInt($scope.leave.overtimeouthour) * 60) + parseInt($scope.leave.overtimeoutmin) ;
          dataPost.startDateInTime = $scope.leave.startDateInTime;
          dataPost.endDateInTime = $scope.leave.endDateInTime;
          dataPost.overtimeIn = diffTwoDateMinutes($scope.leave.startDateInTime, $scope.leave.endDateInTime);
        } else {
          dataPost.typeDesc = $scope.labelType;
          dataPost.startDate = $scope.leave.startDate;
          dataPost.endDate = $scope.leave.endDate;
          dataPost.remark = $scope.leave.remark;
          //dataPost.substituteToEmployment = $scope.leave.substituteToEmployment;

        }

        dataPost.categoryType = $scope.leave.categoryType;
        if (objCategory.directType) {
          var typeSelected = objCategory.listRequestType[0];
          dataPost.type = typeSelected.type;
          dataPost.typeDesc = typeSelected.typeDesc;
        } else {
          dataPost.type = $scope.leave.type;
        }


        dataPost.workflow = "SUBMITAT";
        dataPost.module = "Time Management";
        dataPost.employee = employee;

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/tmrequestheader/verificationleave';

        var data = JSON.stringify(dataPost);

        Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
      } else {
        $scope.warningAlert(messageValidation);
      }
    }

    function verificationForm(leave) {

      if (leave.type == undefined || leave.type == "") {
        messageValidation = "Type can't be empty";
        return false;
      }

      if (leave.remark == undefined || leave.remark == "") {
        messageValidation = "Remark / Reason   can't be empty";
        if ($scope.leaveCategory == 'leave' || $scope.leaveCategory == 'permission')
          messageValidation = "Substitute employee can't be empty";
        return false;
      }

      if ($scope.leaveCategory == 'overtime') {
        if (diffTwoDateTimeMiliSecond(leave.startDateInTime, leave.endDateInTime) <= 0) {
          messageValidation = "End time must be bigger than Start Time";
          return false;
        }

      }

      return true;
    }



    function getCategory(categoryType) {
      var sessTmRequestType = Main.getSession("tmCategoryType");

      for (var i = 0; i < sessTmRequestType.length; i++) {
        if (sessTmRequestType[i].categoryType == categoryType) {
          return sessTmRequestType[i];
          break;
        }

      };
      return undefined;
    }

    function getListType(categoryType) {

      var objCategory = getCategory(categoryType);
      if (objCategory != undefined)
        return objCategory.listRequestType;
      else
        return [];
    }





    function successGetSubstitute(res) {
      $ionicLoading.hide();
      $rootScope.selectEmployeeSubstitute = res.data;
      $scope.selectEmployeeSubstitute = res.data;
    }

    function getEmployeeSubstitute() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/empassignment/byparam?param=organization&page=0&size=100';
      Main.requestApi(accessToken, urlApi, successGetSubstitute, $scope.errorRequest);

    }






    $scope.goToDetails = function (idx, tgl, actualInTime, actualOutTime) {
      $scope.dataDetail = {};
      $scope.dataDetail.startDate = tgl;
      $scope.dataDetail.actualInTime = actualInTime;
      $scope.dataDetail.actualOutTime = actualOutTime;

      $rootScope.dataAbsenceDetail = $scope.dataDetail;
      $state.go('app.editabsencetransaction', {
        'idx': idx
      });
    };


    $scope.searchAbsenceList = function (month, year) {

      month = Main.getIdfromValue(
        $scope.selectMonth,
        month
      );

      getListAbsence(month, year);
    }


    function getListAbsence(month, year) {

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/resInterfaceList?month=' + month + '&year=' + year;
      Main.requestApi(accessToken, urlApi, successAbsence, $scope.errorRequest);
    }


    var successAbsence = function (res) {
      if (res != null) {
        $scope.absenceList = res;
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    }






    function initData() {
      $scope.leave = {
        remark: "",
        startDate: new Date(),
        endDate: new Date(),
        remark: "",
        attendanceIn: new Date(),
        attendanceOut: new Date()
      };
      $scope.balance = {
        balanceEnd: "-",
        balanceUsed: "-"
      };
      $scope.selectType = getListType($stateParams.category);
      $scope.labelSubstitute = "Select Substitute ..";
      $scope.labelType = "Select Type ..";
      $scope.leave.substituteToEmployment = undefined;
      $scope.leave.type = undefined;
      $scope.leave.overtimeinhour = '0';
      $scope.leave.overtimeinmin = '0';
      $scope.leave.overtimeouthour = '0';
      $scope.leave.overtimeoutmin = '0';
      $scope.leave.startDateInTimeDate = new Date();
      $scope.leave.startDateInTimeTime = $filter('date')(new Date(), 'HH:mm');;
      $scope.leave.endDateInTimeDate = new Date();
      $scope.leave.endDateInTimeTime = $filter('date')(new Date(), 'HH:mm');
    }

    function initModule() {
      initData();

      // commented by JUW, change logic
      // getListAbsence($scope.dataMonth, $scope.currentYear);
      var tmpMonth = Main.getIdfromValue(
        $scope.selectMonth,
        $scope.search.month
      );
      getListAbsence(tmpMonth, $scope.search.year);

      if ($rootScope.selectEmployeeSubstitute == undefined) {
        getEmployeeSubstitute();
      } else {
        $scope.selectEmployeeSubstitute = $rootScope.selectEmployeeSubstitute;
      }
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined)
        initModule();
    });

  })

  .controller('ListLeaveCtrl', function ($ionicLoading, $scope, $state, Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.leaves = [];

    $scope.goToDetailLeave = function (id) {
      $state.go("app.detailleave", {
        id: id
      });
    }

    $scope.refresh = function () {
      initMethod();
    }

    var successRequest = function (res) {
      $scope.leaves = res;
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    }

    function initMethod() {
      getListBenefit();
    }


    function getListBenefit() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequest?module=Time Management';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initMethod();

    });
  })

  .controller('DetailLeaveCtrl', function (ionicSuperPopup, $stateParams, $ionicLoading, $ionicLoading, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.isHr = Main.getSession("profile").isHr;
    var id = $stateParams.id;
    $scope.leave = {};
    $scope.header = {};

    function successRequest(res) {
      $scope.header = res;

      $ionicLoading.hide();
      if (res != undefined && res.details.length > 0) {
        $scope.leave = res.details[0];
      }

    }

    var successApprove = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.leaves");
    }
    var sendApproval = function (action, id, reason) {
      var data = {};
      if (action == 'approved')
        data = {
          "id": id,
          "status": action
        };
      else
        data = {
          "id": id,
          "status": action,
          "reasonReject": reason
        };

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/actionapproval';
      var data = JSON.stringify(data);

      Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);

    }

    $scope.confirmCancel = function (approvalId) {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Cancel this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function (isConfirm) {
          if (isConfirm) {
            sendApproval('cancelled', approvalId, "");
          }


        });
    }

    function getDetailHeader(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/tmrequest/' + id;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function initModule() {
      id = $stateParams.id;
      $scope.leave = {};
      getDetailHeader(id);
    }
    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();

    });

  })

  .controller('AddOvertimeCtrl', function (ionicTimePicker, $filter, ionicSuperPopup, $ionicLoading, $rootScope, $scope, $state, Main, ionicDatePicker) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var startDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    var endDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    var employee = Main.getSession("profile").employeeTransient.id;
    $scope.request = {};




    var profile = Main.getSession("profile");

    $scope.npk = profile.employeeTransient.assignment.employeeNo;

    $scope.request.ovtStartDate = new Date();
    $scope.request.ovtEndDate = new Date();
    $scope.request.ovtType = 'REQUEST';
    var unixTimeStart = "";
    var unixDateStart = "";
    var unixTimeEnd = "";
    var unixDateEnd = "";


    var module = "time management";
    var messageValidation = "";



    var timePickerOver1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.request.ovtStartTime = hourString + ":" + minuteString + ":" + "00";;
          unixTimeStart = val;

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 5, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver1 = function () {
      ionicTimePicker.openTimePicker(timePickerOver1);
    };



    var timePickerOver2 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.request.ovtEndTime = hourString + ":" + minuteString + ":" + "00";
          unixTimeEnd = val;

        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 5, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver2 = function () {
      ionicTimePicker.openTimePicker(timePickerOver2);
    };


    var datepicker = {
      callback: function (val) { //Mandatory
        $scope.request.ovtStartDate = val;
        unixDateStart = val;

        getGroupSecurity($scope.npk, val);
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    var datepicker1 = {
      callback: function (val) { //Mandatory
        $scope.request.ovtEndDate = val;

        unixDateEnd = val;

      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };




    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };


    function getGroupSecurity(npk, date) {

      if (date == null)
        date = $filter("date")(new Date(), "yyyy-MM-dd");
      else
        date = $filter("date")(new Date(date), "yyyy-MM-dd");

      var npk = $scope.npk;


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/activeEmpGroup/' + npk + '/' + date;
      Main.requestApi(accessToken, urlApi, successGroup, $scope.errorRequest);



    }


    var successGroup = function (res) {
      if (res != null) {
        $scope.dataDetailGroup = res;
        $ionicLoading.hide();
      }
    }


    function verificationForm(request) {
      var totalStartDate = unixDateStart + unixTimeStart;
      var totalEndDate = unixDateEnd + unixTimeEnd;


      if (request.ovtStartDate == undefined) {
        messageValidation = "Overtime Start Date can't be empty";
        return false;
      } else if (request.ovtEndDate == undefined) {
        messageValidation = "Overtime End Date can't be empty";
        return false;
      } else if (request.ovtStartTime == undefined) {
        messageValidation = "Overtime Start Time can't be empty";
        return false;
      } else if (request.ovtEndTime == undefined) {
        messageValidation = "Overtime End Time can't be empty";
        return false;
      } else if (request.remark == undefined || request.remark == '') {
        messageValidation = "Description can't be empty";
        return false;
      } else if (request.target == undefined || request.target == '') {
        messageValidation = "Target can't be empty";
        return false;
      } else if (unixDateStart == unixDateEnd) {
        if (request.ovtStartTime > request.ovtEndTime) {
          messageValidation = "End time must be greater than start time";
          return false;
        }
      }
      return true;


    }



    function sendData() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;

      var data = $scope.request;
      data.ovtStartDate = $filter('date')(new Date(data.ovtStartDate), "yyyy-MM-dd");
      data.ovtEndDate = $filter('date')(new Date(data.ovtEndDate), "yyyy-MM-dd");
      data = JSON.stringify(data);

      var urlApi = Main.getUrlApi() + '/api/user/ovtRequest';

      Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
    }

    $scope.submitForm = function () {

      if (verificationForm($scope.request)) {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure you want to submit Overtime Planning?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }
          });
      } else {
        $scope.warningAlert(messageValidation);
      }
    }






    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAddLeaveCtrlCtrl = true;
      $state.go('app.addleave', {
        'category': 'Overtime'
      });
    }



    function initModule() {
      getGroupSecurity();
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {

      initModule();
    });

  })


  .controller('OvertimeRealizationCtrl', function ($ionicLoading, $ionicLoading, $rootScope, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var i = 0;
    var size = Main.getDataDisplaySize();

    $scope.overtimeRealizationList = [];
    $scope.isLoadmoreShow = false;

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshOvertimeRealizationCtrl) {
        initMethod();
      }
      $rootScope.refreshOvertimeRealizationCtrl = false;

    });


    $scope.LoadMoreShow = function () {
      i++;
      getListOvertime(i);
    }


    $scope.refresh = function () {
      initMethod();
    }

    function initMethod() {
      i = 0; // reset pagging
      $scope.overtimeRealizationList = [];
      getListOvertime(0);
    }



    $scope.goToDetails = function (id) {
      $state.go('app.addovertimerealization', {
        'id': id
      });
    };

    function getListOvertime(page) {
      if (page == null || page === 'undefined') {
        page = 0;
      }

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;

      var urlApi = Main.getUrlApi() + '/api/user/ovtRequest/allowedrequest?page=' + page + '&size=' + size;
      Main.requestApi(accessToken, urlApi, sucessOvertime, $scope.errorRequest);

    }

    var sucessOvertime = function (res) {
      $scope.overtimeRealizationList = res;
      $scope.isLast = res.last;

      if ($scope.overtimeRealizationList.length == res.totalRecord) {
        $scope.isLoadmoreShow = false;
      } else if ($scope.overtimeRealizationList.length < size) {
        $scope.isLoadmoreShow = false;
      } else {
        $scope.isLoadmoreShow = true;
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    }


    function initModule() {
      getListOvertime(0);
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });





  })

  .controller('AddOvertimeRealizationCtrl', function (ionicTimePicker, $filter, $stateParams, $ionicLoading, $rootScope, $scope, $state, Main, ionicDatePicker, ionicSuperPopup) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var employee = Main.getSession("profile").employeeTransient.id;
    var profile = Main.getSession("profile");

    $scope.realization = {};
    $scope.detailActualInOut = {};

    $scope.realization.ovtStartDate = new Date();
    $scope.realization.ovtEndDate = new Date();
    $scope.realization.ovtType = 'REALIZATION';
    $scope.realization.requestId = $stateParams.id;
    $scope.dataCheck = 0;


    var messageValidation = "";
    var unixDateStart = "";
    var unixDateEnd = "";



    var timePickerOver1 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.realization.ovtStartTime = hourString + ":" + minuteString + ":" + "00";;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 5, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver1 = function () {
      ionicTimePicker.openTimePicker(timePickerOver1);
    };



    var timePickerOver2 = {
      callback: function (val) {

        if (typeof (val) === 'undefined') {

        } else {
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          var hourString = "" + hours;
          var minuteString = "" + minutes;

          if (hourString.length == 1)
            hourString = "0" + hours;

          if (minuteString.length == 1)
            minuteString = "0" + minutes;

          $scope.realization.ovtEndTime = hourString + ":" + minuteString + ":" + "00";;
        }
      },
      inputTime: 32400, //Optional
      format: 24, //Optional
      step: 5, //Optional
      setLabel: 'Set' //Optional
    };

    $scope.openTimePickerOver2 = function () {
      ionicTimePicker.openTimePicker(timePickerOver2);
    };


    var datepicker = {
      callback: function (val) { //Mandatory
        $scope.realization.ovtStartDate = val;
        unixDateStart = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    var datepicker1 = {
      callback: function (val) { //Mandatory
        $scope.realization.ovtEndDate = val;
        unixDateEnd = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };




    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };



    function getDetailRequest() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var id = $scope.realization.requestId;

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/ovtRequest/' + id;
      Main.requestApi(accessToken, urlApi, successDetailRequest, $scope.errorRequest);



    }


    var successDetailRequest = function (res) {
      if (res != null) {
        $scope.detailRequest = res;

        // assign detail request
        $scope.realization.ovtStartDate = res.startDate;
        $scope.realization.ovtEndDate = res.endDate;
        $scope.realization.ovtStartTime = res.startTime;
        $scope.realization.ovtEndTime = res.endTime;
        $scope.realization.remark = res.remark;
        $scope.realization.target = res.target;

        var startDate = $filter("date")(new Date(res.startDate), "yyyy-MM-dd");
        getActualinoutRequest(startDate);
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    }


    function getActualinoutRequest(date) {


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });


      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/resInterface?date=' + date;

      Main.requestApi(accessToken, urlApi, successgetActualinoutRequest, $scope.errorRequest);



    }


    var successgetActualinoutRequest = function (res) {
      if (Object.keys(res).length > 0) {
        $scope.detailActualInOut = res;

        if (res.actualOutTime != null || res.actualOutTime != '') {
          $scope.dataCheck = 1;
        } else {
          $scope.dataCheck = 0;
        }
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    }









    function verificationForm(realization) {
      //('bla bla');
      //($scope.detailActualInOut);
      // if (realization.ovtStartDate == undefined) {
      //   messageValidation = "Overtime Start Date can't be empty";
      //   return false;
      // } else if (realization.ovtEndDate == undefined) {
      //   messageValidation = "Overtime End Date can't be empty";
      //   return false;
      // } else if (realization.ovtStartTime == undefined) {
      //   messageValidation = "Overtime Start Time can't be empty";
      //   return false;
      // } else if (realization.ovtEndTime == undefined) {
      //   messageValidation = "Overtime End Time can't be empty";
      //   return false;
      // } else if (realization.remark == undefined || realization.remark == '') {
      //   messageValidation = "Description can't be empty";
      //   return false;
      // } else if (realization.target == undefined || realization.target == '') {
      //   messageValidation = "Target can't be empty";
      //   return false;
      // } else if (realization.ovtStartTime < $scope.detailRequest.startTime) {
      //   messageValidation = "Please input time after " + $filter('timeWithoutSecond')($scope.detailRequest.startTime);
      //   return false;
      // } else if (realization.ovtEndTime > $scope.detailRequest.endTime) {
      //   messageValidation = "Please input time before " + $filter('timeWithoutSecond')($scope.detailRequest.endTime);
      //   return false;
      // } else if ($scope.detailActualInOut == null || $scope.detailActualInOut == undefined || $scope.detailActualInOut == '' || Object.keys($scope.detailActualInOut).length === 0 || $scope.detailActualInOut.actualOutTime == null) {
      //   messageValidation = "Cannot Submit Request. Your Actual In/Out is Empty. ";
      //   return false;
      // }
      // }else if (unixDateStart == unixDateEnd) {
      //   if(realization.ovtStartTime > realization.ovtEndTime){
      //     messageValidation = "End time must be greater than start time";
      //     return false;
      //   }
      // }
      if ($scope.detailActualInOut == null || $scope.detailActualInOut == undefined || $scope.detailActualInOut == '' || Object.keys($scope.detailActualInOut).length === 0 || $scope.detailActualInOut.actualOutTime == null || $scope.detailActualInOut.actualInTime == null) {
        messageValidation = "Cannot Submit Request. Your Actual In/Out is Empty. ";
        return false;
      }


      return true;
    }



    function sendData() {

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });


      var accessToken = Main.getSession("token").access_token;
      var data = JSON.stringify($scope.realization);

      var urlApi = Main.getUrlApi() + '/api/user/ovtRequest';

      Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

    }


    $scope.submitForm = function () {
      $scope.realization.actualInTime = $filter('date')(new Date($scope.detailActualInOut.actualInTime), 'HH:mm:ss');
      $scope.realization.actualOutTime = $filter('date')(new Date($scope.detailActualInOut.actualOutTime), 'HH:mm:ss');
      $scope.realization.resInterfaceDtlId = $scope.detailActualInOut.id;
      $scope.realization.shiftId = $scope.detailActualInOut.shiftId;
      $scope.realization.actualInDate = $filter('date')(new Date($scope.detailActualInOut.actualInTime), 'yyyy-MM-dd');
      $scope.realization.actualOutDate = $filter('date')(new Date($scope.detailActualInOut.actualOutTime), 'yyyy-MM-dd');




      if (verificationForm($scope.realization)) {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure you want to submit  Overtime Realization?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }
          });
      } else {
        $scope.warningAlert(messageValidation);
      }

    }



    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshOvertimeRealizationCtrl = true;
      $state.go('app.overtimerealization', {
        'category': 'overtime'
      });
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      getDetailRequest();

    });

  })


  .controller('EditabsencetransactionCtrl',
    function (
      $filter,
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      Main,
      $ionicActionSheet,
      $cordovaCamera,
      appService
    ) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      var employee = Main.getSession("profile").employeeTransient.id;
      var profile = Main.getSession("profile");
      $scope.appMode = Main.getAppMode();



      $scope.absenceDetail = {};
      $scope.absenceDetail.attachments = [];
      $scope.absenceDetail.employee = Main.getSession("profile").employeeTransient.id;
      $scope.defaultImage = "img/placeholder.png";
      $scope.images = [];





      if ($rootScope.dataAbsenceDetail != null) {
        $scope.absenceDetail.startDate = $filter("date")(new Date($rootScope.dataAbsenceDetail.startDate), "yyyy-MM-dd");
        $scope.absenceDetail.actualInTime = $rootScope.dataAbsenceDetail.actualInTime;
        $scope.absenceDetail.actualOutTime = $rootScope.dataAbsenceDetail.actualOutTime;
      }



      $scope.listType = getListType("Absence Transaction");


      function getListType(categoryType) {

        var objCategory = getCategory(categoryType);
        if (objCategory != undefined)
          return objCategory.listRequestType;
        else
          return [];
      }


      function getCategory(categoryType) {
        var sessTmRequestType = Main.getSession("tmCategoryType");

        for (var i = 0; i < sessTmRequestType.length; i++) {
          if (sessTmRequestType[i].categoryType == categoryType) {
            return sessTmRequestType[i];
            break;
          }

        };
        return undefined;
      }


      $scope.removeChoice = function () {
        var lastItem = $scope.absenceDetail.attachments.length - 1;
        $scope.absenceDetail.attachments.splice(lastItem);
        $scope.images.splice(lastItem);

      };

      $scope.addPicture = function () {
        if ($scope.images.length > 2) {
          alert("Only 3 pictures can be upload");
          return false;
        }
        $ionicActionSheet.show({
          buttons: [{
              text: "Take Picture"
            },
            {
              text: "Select From Gallery"
            }
          ],
          buttonClicked: function (index) {
            switch (index) {
              case 0: // Take Picture
                document.addEventListener(
                  "deviceready",
                  function () {
                    $cordovaCamera.getPicture(appService.getCameraOptions()).then(
                      function (imageData) {
                        $scope.images.push({
                          image: "data:image/jpeg;base64," + imageData
                        });
                        $scope.absenceDetail.attachments.push({
                          image: imageData
                        });
                      },
                      function (err) {
                        appService.showAlert(
                          "Error",
                          err,
                          "Close",
                          "button-assertive",
                          null
                        );
                      }
                    );
                  },
                  false
                );

                break;
              case 1: // Select From Gallery
                document.addEventListener(
                  "deviceready",
                  function () {
                    $cordovaCamera
                      .getPicture(appService.getLibraryOptions())
                      .then(
                        function (imageData) {
                          $scope.images.push({
                            image: "data:image/jpeg;base64," + imageData
                          });
                          $scope.absenceDetail.attachments.push({
                            image: imageData
                          });
                        },
                        function (err) {
                          appService.showAlert(
                            "Error",
                            err,
                            "Close",
                            "button-assertive",
                            null
                          );
                        }
                      );
                  },
                  false
                );
                break;
            }
            return true;
          }
        });
      };


      function verificationForm(absenceDetail) {


        if (absenceDetail.type == undefined || absenceDetail.type == "") {
          messageValidation = "Type can't be empty";
          return false;
        }

        if (absenceDetail.type === 'SWOD') { // type sakit dengan surat dokter

          if (absenceDetail.remark == undefined || absenceDetail.remark == "") {
            messageValidation = "Remark can't be empty";
            return false;
          }


        }



        if (absenceDetail.type === 'SWD') {
          if (absenceDetail.remark == undefined || absenceDetail.remark == "") {
            messageValidation = "Remark can't be empty";
            return false;
          }


          if (absenceDetail.attachments == "" || absenceDetail.attachments == undefined) {
            messageValidation = "Attachments can't be empty";
            return false;
          }

        }



        if (absenceDetail.type === 'PDT') {

          if (absenceDetail.remark == undefined || absenceDetail.remark == "") {
            messageValidation = "Remark can't be empty";
            return false;
          }

        }


        if (absenceDetail.type === 'PPLC') {

          if (absenceDetail.remark == undefined || absenceDetail.remark == "") {
            messageValidation = "Remark can't be empty";
            return false;
          }

        }

        return true;
      }




      $scope.submitForm = function () {
        if (verificationForm($scope.absenceDetail)) {
          $ionicLoading.show({
            template: "<ion-spinner></ion-spinner>"
          });
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + "/api/user/tmrequestheader/absencetransaction";
          var attachment = [];
          if ($scope.absenceDetail.attachments.length > 0) {
            for (
              var i = $scope.absenceDetail.attachments.length - 1; i >= 0; i--
            ) {
              var objAttachment = {
                image: null
              };
              if ($scope.appMode == "mobile") {
                objAttachment = {
                  image: $scope.absenceDetail.attachments[i].image
                };
              } else {
                if (
                  $scope.absenceDetail.attachments[i].compressed.dataURL !=
                  undefined
                ) {
                  var webImageAttachment = $scope.absenceDetail.attachments[
                    i
                  ].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
                  objAttachment = {
                    image: webImageAttachment
                  };
                }
              }
              attachment.push(objAttachment);
            }
          }

          $scope.absenceDetail.attachments = attachment;

          var data = JSON.stringify($scope.absenceDetail);

          Main.postRequestApi(
            accessToken,
            urlApi,
            data,
            successRequest,
            $scope.errorRequest
          );
        } else {
          $scope.warningAlert(messageValidation);
        }
      };


      var successRequest = function (res) {
        $ionicLoading.hide();
        $scope.successAlert(res.message);
        $state.go('app.addleave', {
          'category': 'Absence Transaction'
        });
      };






      $scope.$on('$ionicView.beforeEnter', function (event, data) {
        // if (data.direction != undefined && data.direction != 'back')
        // getDetailRequest();

      });

    })
