angular
  .module("admin.securitygroup.controller", [])
  .controller("AdminSecurityGroupCtrl", function (
    $scope,
    $state,
    $ionicLoading,
    $ionicModal,
    $ionicPopup,
    $filter,
    globalConstant,
    ionicDatePicker,
    Main
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var i = 0; // paging filter
    var size = Main.getDataDisplaySize();
    $scope.globalConstant = globalConstant;

    $ionicModal.fromTemplateUrl('modalFilter.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalFilter = modal;
    });

    var datePickerAsOfDate = {
      callback: function (val) { //Mandatory
        $scope.filter.asOfDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: globalConstant.dateFormat,
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDPAsOfDate = function () {
      ionicDatePicker.openDatePicker(datePickerAsOfDate);
    };

    $scope.gotoDetailSecurityGroup = function (id) {
      $state.go('app.adminsecuritygroupdetail', {
        'id': id
      });
    };

    $scope.confirmDeleteSecurityGroup = function (id) {
      $ionicPopup.confirm({
        title: 'Confirm',
        template: '<h5>Are you sure to delete this Security Groups?</h5>',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var data = {
            id: id,
          };
          data = JSON.stringify(data);
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/admin/secGroups/delete';
          Main.postRequestApi(accessToken, urlApi, data, successDeleteSecurityGroup, $scope.errorRequest);
        }
      });
    };

    $scope.openModalFilter = function () {
      $scope.modalFilter.show();
    };

    $scope.closeModalFilter = function () {
      $scope.modalFilter.hide();
    };

    var successDeleteSecurityGroup = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSecurityGroup(0);
    };

    var successRequestListSecurityGroup = function (res) {
      if (res != null) {
        $scope.listSecurityGroup = res;
      }
      $ionicLoading.hide();
    };

    function getListSecurityGroup(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/secGroups?page=" + page + "&size=" + size;

      if ($scope.filter.employee != undefined && $scope.filter.employee != '') {
        urlApi = urlApi + '&ppl=' + $scope.filter.employee;
      }
      Main.requestApi(accessToken, urlApi, successRequestListSecurityGroup, $scope.errorRequest);
    }

    function initModule() {
      var today = new Date();

      $scope.listSecurityGroup = [];
      $scope.filter = {
        asOfDate: $filter("date")(today, globalConstant.dateFormatToDB),
      };
      getListSecurityGroup(0);
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });

  })

  .controller("AdminSecurityGroupDetailCtrl", function (
    $scope,
    $state,
    $ionicLoading,
    $ionicModal,
    $ionicPopup,
    $stateParams,
    $ionicScrollDelegate,
    $ionicHistory,
    $timeout,
    globalConstant,
    ionicSuperPopup,
    Main
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var size = Main.getDataDisplaySize();
    var indexPaging = 0;

    $scope.doResetPaging = function () {
      indexPaging = 0;
    };

    $ionicModal.fromTemplateUrl('modalSecurityGroupDetail.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalSecurityGroupDetail = modal;
    });

    $ionicModal.fromTemplateUrl('modalSecurityMatrix.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalSecurityMatrix = modal;
    });

    $ionicModal.fromTemplateUrl('modalDivision.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalDivision = modal;
    });

    $ionicModal.fromTemplateUrl('modalEmployee.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalEmployee = modal;
    });

    $scope.openModalEmployee = function () {
      $scope.doResetPaging();
      $scope.searchByEmployee(0);
      $scope.modalEmployee.show();
    };

    $scope.closeModalEmployee = function () {
      $scope.modalEmployee.hide();
      $scope.findByNpk.search = '';
    };

    $scope.getValueEmployee = function (employee) {
      $scope.formSecurityMatrix.employeeName = employee.fullName + ' - ' + employee.employeeNo;
      $scope.formSecurityMatrix.employeeId = employee.employee;
      $scope.closeModalEmployee();

      // reset search position
      $scope.findByNpk.search = '';

      //reset
      $scope.listEmployee = [];
    };

    $scope.loadMoreEmployee = function () {
      indexPaging++;
      $scope.listEmployee = [];
      $scope.searchByEmployee(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevEmployee = function () {
      indexPaging--;
      $scope.listEmployee = [];
      $scope.searchByEmployee(indexPaging);
    };

    $scope.searchByEmployee = function (page) {
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/employee/find?page=" + page + "&size=" +
        size + "&name=" + encodeURIComponent($scope.findByNpk.search);
      Main.requestApi(accessToken, urlApi, successRequestEmployee, $scope.errorRequest);
    };

    $scope.resetEmployee = function () {
      $scope.formSecurityMatrix.employeeName = '';
      $scope.formSecurityMatrix.employeeId = '';
    };

    var successRequestEmployee = function (res) {
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $scope.listEmployee = [];

      $scope.listEmployee = res.content;
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $ionicScrollDelegate.scrollTop();
    };

    $scope.openModalDivision = function () {
      $scope.doResetPaging();
      $scope.searchByDivision(0);
      $scope.modalDivision.show();
    };

    $scope.closeModalDivision = function () {
      $scope.modalDivision.hide();
      $scope.findByDivision.search = '';
    };

    $scope.getValueDivision = function (division) {
      $scope.formSecurityMatrix.division = division;
      $scope.closeModalDivision();

      // reset search position
      $scope.findByDivision.search = '';

      //reset
      $scope.listDivision = [];
    };

    $scope.loadMoreDivision = function () {
      indexPaging++;
      $scope.listDivision = [];
      $scope.searchByDivision(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevDivision = function () {
      indexPaging--;
      $scope.listDivision = [];
      $scope.searchByDivision(indexPaging);
    };

    $scope.searchByDivision = function (page) {
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/division/find?page=" + page + "&size=" +
        size + "&name=" + encodeURIComponent($scope.findByDivision.search);
      Main.requestApi(accessToken, urlApi, successRequestDivision, $scope.errorRequest);
    };

    $scope.resetDivision = function () {
      $scope.formSecurityMatrix.division = '';
    };

    var successRequestDivision = function (res) {
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $scope.listDivision = [];

      $scope.listDivision = res.content;
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $ionicScrollDelegate.scrollTop();
    };

    $scope.addSecurityGroupDetail = function () {
      $scope.formSecurityGroupDetail = {
        authority: '',
        groupId: '',
      };
      $scope.modalSecurityGroupDetail.show();
    };

    $scope.addSecurityMatrix = function () {
      $scope.formSecurityMatrix = {
        authority: '',
        groupId: '',
      };
      $scope.modalSecurityMatrix.show();
    };

    function getListAuthority() {
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/talentsParameter?groupKey=AUTHORITY_LIST";
      Main.requestApi(accessToken, urlApi, successListAuthority, $scope.errorRequest);
    }

    var successListAuthority = function (res) {
      if (res != null) {
        $scope.optAuthority = res;
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    var successSubmitFormSecurityGroup = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);

      // create new
      if ($scope.securityGroupId == 0) {
        if (res.id != null && res.id != undefined) {
          $scope.goBack('app.adminsecuritygroup');
        }
      }
    };

    var successSubmitFormSecurityGroupDetail = function (res) {
      $scope.closeModal();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.listSecurityGroupDetail = [];
      getListSecurityGroupDetail();
    };

    var successSubmitFormSecurityMatrix = function (res) {
      $scope.closeModalSecurityMatrix();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.listSecurityMatrix = [];
      getListSecurityMatrix();
    };


    var successRequestListSecurityGroupDetail = function (res) {
      if (res != null) {
        $scope.listSecurityGroupDetail = res;
      }
      $ionicLoading.hide();
    };

    var successDeleteSecurityGroupDetail = function (res) {
      $scope.listSecurityGroupDetail = [];
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSecurityGroupDetail();
    };

    var successDeleteSecurityMatrix = function (res) {
      $scope.listSecurityMatrix = [];
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSecurityMatrix();
    };

    var successRequestSecurityGroupDetailById = function (res) {
      if (res != null && res != '') {
        $scope.formSecurityGroupDetail = res;
        $scope.formSecurityGroupDetail.talentsParameter = res.talentsParameter[0];
      }
      $ionicLoading.hide();
    };

    var successRequestSecurityGroupDetail = function (res) {
      if (res != null && res != '') {
        $scope.frmSecurityGroup = res;
      }
      $ionicLoading.hide();
    };

    var successRequestSecurityMatrixById = function (res) {
      if (res != null && res != '') {
        $scope.formSecurityMatrix = res;

        if (res.division != null) {
          $scope.formSecurityMatrix.division = res.division;
        }

        if (res.vwEmpAssignment != null) {
          $scope.formSecurityMatrix.employeeName = res.vwEmpAssignment.fullName + ' - ' + res.vwEmpAssignment.employeeNo;
        }
      }
      $ionicLoading.hide();
    };

    var successRequestListSecurityMatrix = function (res) {
      if (res != null) {
        $scope.listSecurityMatrix = res;
      }
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $ionicScrollDelegate.resize();
    };

    function getSecurityGroupDetail() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/secGroups/" + $scope.securityGroupId;
      Main.requestApi(accessToken, urlApi, successRequestSecurityGroupDetail, $scope.errorRequest);
    }

    function getListSecurityGroupDetail() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/secGroups/" + $scope.securityGroupId + "/detail";
      Main.requestApi(accessToken, urlApi, successRequestListSecurityGroupDetail, $scope.errorRequest);
    }

    function getSecurityGroupDetailById(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/admin/secGroups/' + $scope.securityGroupId + '/detail/' + id;
      Main.requestApi(accessToken, urlApi, successRequestSecurityGroupDetailById, $scope.errorRequest, $scope.closeModal);
    }

    function getListSecurityMatrix(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/secGroups/" + $scope.securityGroupId + "/matrix";

      // if ($scope.filter.name != undefined && $scope.filter.name != '') {
      //   urlApi = urlApi + '&name=' + $scope.filter.name;
      // }
      Main.requestApi(accessToken, urlApi, successRequestListSecurityMatrix, $scope.errorRequest);
    }

    function getSecurityMatrixById(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/admin/secGroups/' + $scope.securityGroupId + '/matrix/' + id;
      Main.requestApi(accessToken, urlApi, successRequestSecurityMatrixById, $scope.errorRequest, $scope.closeModalSecurityMatrix);
    }

    function verificationForm(form) {
      return true;
    }

    function initModule() {
      $scope.securityGroupId = $stateParams.id;

      $scope.listSecurityGroupDetail = [];
      $scope.listSecurityMatrix = [];
      $scope.globalConstant = globalConstant;

      $scope.findByDivision = {
        search: ""
      };

      $scope.findByNpk = {
        search: ""
      };

      $scope.module = {};
      $scope.frmSecurityGroup = {
        name: '',
        description: '',
        activeFlag: true,
      };
      getListAuthority();
      $scope.chooseTab('general');
    }

    $scope.editSecurityGroupDetail = function (id) {
      getSecurityGroupDetailById(id);
      $scope.modalSecurityGroupDetail.show();
    };

    $scope.editSecurityMatrix = function (id) {
      getSecurityMatrixById(id);
      $scope.modalSecurityMatrix.show();
    };

    $scope.closeModal = function () {
      $scope.modalSecurityGroupDetail.hide();
    };

    $scope.closeModalSecurityMatrix = function () {
      $scope.modalSecurityMatrix.hide();
    };

    $scope.confirmDeleteSecurityGroupDetail = function (id) {
      $ionicPopup.confirm({
        title: 'Confirm',
        template: '<h5>Are you sure to delete this Security Group Detail?</h5>',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var data = {
            id: id,
          };
          data = JSON.stringify(data);
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/admin/secGroupsDetail/delete';
          Main.postRequestApi(accessToken, urlApi, data, successDeleteSecurityGroupDetail, $scope.errorRequest, $scope.closeModal);
        }
      });
    };

    $scope.confirmDeleteSecurityMatrix = function (id) {
      $ionicPopup.confirm({
        title: 'Confirm',
        template: '<h5>Are you sure to delete this Security Matrix?</h5>',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var data = {
            id: id,
          };
          data = JSON.stringify(data);
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/admin/secGroupsMatrix/delete';
          Main.postRequestApi(accessToken, urlApi, data, successDeleteSecurityMatrix, $scope.errorRequest, $scope.closeModalSecurityMatrix);
        }
      });
    };

    $scope.submitFormSecurityGroupDetail = function () {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/secGroupsDetail";

      var data = $scope.formSecurityGroupDetail;
      data.groupId = $scope.securityGroupId;
      data.authority = data.talentsParameter.key;
      data = JSON.stringify(data);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successSubmitFormSecurityGroupDetail,
        $scope.errorRequest,
        $scope.closeModal
      );
    };

    $scope.submitFormSecurityMatrix = function () {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/secGroupsMatrix";

      var data = $scope.formSecurityMatrix;
      data.groupId = $scope.securityGroupId;
      data = JSON.stringify(data);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successSubmitFormSecurityMatrix,
        $scope.errorRequest,
        $scope.closeModalSecurityMatrix()
      );
    };

    $scope.chooseTab = function (tab) {
      $scope.module.type = tab;

      if (tab == 'general') {
        $scope.data = {};
        if ($scope.approvalGroupId != 0) {
          getSecurityGroupDetail();
        }
      } else if (tab == 'detail') {
        $scope.listSecurityGroupDetail = [];
        getListSecurityGroupDetail();
      } else if (tab == 'matrix') {
        $scope.listSecurityMatrix = [];
        getListSecurityMatrix(0);
      }
    };

    $scope.submitFormSecurityGroup = function () {
      if (verificationForm($scope.frmSecurityGroup)) {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });

        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() + "/api/admin/secGroups/";

        var data = $scope.frmSecurityGroup;
        data.id = $scope.frmSecurityGroup.id == '' ? '' : $scope.frmSecurityGroup.id;
        data = JSON.stringify(data);

        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successSubmitFormSecurityGroup,
          $scope.errorRequest
        );
      } else {
        $scope.warningAlert(messageValidation);
      }

    };

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });

  })

;
