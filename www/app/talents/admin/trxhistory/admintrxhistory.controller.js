angular
  .module("admin.trxhistory.controllers", [])
  .controller('AdminRequestSubCtrl',
    function (
      ionicDatePicker,
      $filter,
      globalConstant,
      $ionicModal,
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      $stateParams,
      $ionicScrollDelegate,
      Main
    ) {
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      $scope.module = {};
      var submodule = $stateParams.submodule;
      $scope.requests = [];
      $scope.listRequests = [];
      $scope.filterRequest = {};
      $scope.globalConstant = globalConstant;
      $scope.submodule = submodule;


      var i = 0;
      var size = Main.getDataDisplaySize();
      $scope.isLoadMoreBenefitShow = false;

      var startDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterRequest.startDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openStartDatePicker = function () {
        ionicDatePicker.openDatePicker(startDatePicker);
      };

      var endDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterRequest.endDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openEndDatePicker = function () {
        ionicDatePicker.openDatePicker(endDatePicker);
      };

      $scope.loadMoreBenefit = function () {
        i++;
        getMyRequest(submodule, i);
      };

      $scope.gotoDetailRequest = function (obj) {
        var targetApp = "";

        switch ($scope.submodule) {
          case "SPD Advance":
            targetApp = "app.adminrequestspdadvance";
            break;
          case "SPD Realization":
            targetApp = "app.adminrequestspdrealization";
            break;
        }

        $state.go(targetApp, {
          'id': obj.dataApprovalId
        });
      };

      $scope.refresh = function () {
        getMyRequest(submodule, 0);
      };

      $ionicModal.fromTemplateUrl('modalMyRequest.html', {
        id: '0',
        scope: $scope
      }).then(function (modal) {
        $scope.modalMyRequest = modal;
      });

      $scope.closeModalMyRequest = function () {
        $scope.isLoadMoreShow = false;
        $scope.noDataShow = false;
        $scope.modalMyRequest.hide();
      };

      $scope.openModalMyRequest = function () {
        $scope.isLoadMoreShow = false;
        $scope.noDataShow = false;
        $scope.modalMyRequest.show();
      };

      var successRequest = function (res) {
        if (i == 0) {
          $scope.listRequests = res;
        } else {
          $scope.listRequests.content = $scope.listRequests.content.concat(res.content);
        }

        if ($scope.module.type == 'benefit') {
          if (!res.last)
            $scope.isLoadMoreBenefitShow = true;
          else
            $scope.isLoadMoreBenefitShow = false;
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.resize();
      };

      function getMyRequest(module, page) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = "";

        var tmpModule = module.toLowerCase();
        switch (module) {
          case "SPD Advance":
            tmpModule = 'payroll';
            break;
          case "SPD Realization":
            tmpModule = 'employeeGoal';
            break;
        }

        urlApi = Main.getUrlApi() + '/api/admin/dataApproval/' + tmpModule + '/myrequest?page=' + page + '&size=' + size;

        if ($scope.filterRequest.ppl != undefined && $scope.filterRequest.ppl != '') {
          urlApi = urlApi + '&ppl=' + $scope.filterRequest.ppl;
        }

        if ($scope.filterRequest.status != undefined && $scope.filterRequest.status != '') {
          urlApi = urlApi + '&status=' + $scope.filterRequest.status;
        }

        if ($scope.filterRequest.startDate != undefined && $scope.filterRequest.startDate != '') {
          urlApi = urlApi + '&requestDateStart=' + $filter("date")($scope.filterRequest.startDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterRequest.endDate != undefined && $scope.filterRequest.endDate != '') {
          urlApi = urlApi + '&requestDateEnd=' + $filter("date")($scope.filterRequest.endDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterRequest.action != undefined && $scope.filterRequest.action != '') {
          urlApi = urlApi + '&action=' + $scope.filterRequest.action;
        }

        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      $scope.doFilterRequest = function () {
        // check if date filter already fill as expected
        if (($scope.filterRequest.startDate == null && $scope.filterRequest.endDate == null) ||
          ($scope.filterRequest.startDate != null && $scope.filterRequest.endDate != null)
        ) {
          getMyRequest(submodule, 0);
          $scope.closeModalMyRequest();
        }

      };

      function initMethod() {
        $scope.requests = [];
        $scope.listRequests = [];
        var date = new Date();
        var vFirstDate = new Date(date.getFullYear(), date.getMonth(), 1);
        var vLastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        $scope.filterRequest.startDate = vFirstDate;
        $scope.filterRequest.endDate = vLastDate;
        getMyRequest(submodule, 0);
      }

      $scope.$on('$ionicView.beforeEnter', function (event, data) {
        if (data.direction != undefined && data.direction != 'back') {
          initMethod();
        }
        if ($rootScope.refreshMyRequest) {
          initMethod();
        }
        $rootScope.refreshMyRequest = false;
      });
    })

  .controller('AdminRequestSpdAdvanceCtrl',
    function (
      globalConstant,
      ionicSuperPopup,
      $ionicLoading,
      $stateParams,
      $rootScope,
      $scope,
      $state,
      Main) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      var id = $stateParams.id;
      $scope.detail = [];
      $scope.attachment = "img/placeholder.png";
      $scope.globalConstant = globalConstant;
      $scope.module = {};

      $scope.action = "adminrequest";

      $scope.chooseTab = function (tab) {
        i = 0;
        $scope.module.type = tab;
        $scope.detail = [];
        getDetailRequest(tab, i);
      };

      $scope.downloadDocs = function (location) {
        window.open(encodeURI(location), "_system", "location=yes");
        return false;
      };

      var successApprove = function (res) {
        $ionicLoading.hide();
        $scope.successAlert(res.message);
        $rootScope.refreshMyRequest = true;
        $scope.goBack('app.adminrequestsub', {
          'submodule': 'Absence'
        });
      };

      var sendApproval = function (action, id, reason) {

        var data = {
          "dataApprovalId": id,
          "status": action,
          "reasonReject": ""
        };

        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/admin/dataApproval/actionApproval2';
        data = JSON.stringify(data);
        Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);

      };

      $scope.confirmCancel = function () {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure want to Cancel this request ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendApproval('Canceled', id, "");
            }
          });
      };

      var successRequest = function (res) {
        $scope.detail = res;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getDetailRequest(module) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
          $state.go("login");
        }

        var accessToken = Main.getSession("token").access_token;
        var urlApi = '';
        if (module == 'general') {
          urlApi = Main.getUrlApi() + '/api/admin/dataApproval/absence/myrequest/' + id;
        } else if (module == 'apprHistory') {
          urlApi = Main.getUrlApi() + '/api/admin/dataApproval/absence/historyApprovalById/' + id;
        }
        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      function initModule() {
        $scope.isYourData = true;
        $scope.detail = [];
        $scope.attachment = "img/placeholder.png";
        $scope.chooseTab('general');
      }

      $scope.$on('$ionicView.beforeEnter', function () {
        initModule();
      });

    })

  .controller('AdminRequestSpdRealizationCtrl',
    function (
      globalConstant,
      ionicSuperPopup,
      $ionicLoading,
      $stateParams,
      $rootScope,
      $scope,
      $state,
      Main) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      var id = $stateParams.id;
      $scope.detail = [];
      $scope.attachment = "img/placeholder.png";
      $scope.globalConstant = globalConstant;
      $scope.module = {};

      $scope.action = "adminrequest";

      $scope.chooseTab = function (tab) {
        i = 0;
        $scope.module.type = tab;
        $scope.detail = [];
        getDetailRequest(tab, i);
      };

      var successApprove = function (res) {
        $ionicLoading.hide();
        $scope.successAlert(res.message);
        $rootScope.refreshMyRequest = true;
        $scope.goBack('app.adminrequestsub', {
          'submodule': 'Long Leave Cash'
        });
      };

      var sendApproval = function (action, id, reason) {
        var data = {
          "dataApprovalId": id,
          "status": action,
          "reasonReject": ""
        };

        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/admin/dataApproval/actionApproval2';
        data = JSON.stringify(data);
        Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);
      };

      $scope.confirmCancel = function () {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure want to Cancel this request ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendApproval('Canceled', id, "");
            }
          });
      };

      var successRequest = function (res) {
        $scope.detail = res;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getDetailRequest(module) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
          $state.go("login");
        }

        var accessToken = Main.getSession("token").access_token;
        var urlApi = '';
        if (module == 'general') {
          urlApi = Main.getUrlApi() + '/api/admin/dataApproval/overtime/myrequest/' + id;
        } else if (module == 'apprHistory') {
          urlApi = Main.getUrlApi() + '/api/admin/dataApproval/overtime/historyApprovalById/' + id;
        }
        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      function initModule() {
        $scope.detail = [];
        $scope.chooseTab('general');
      }

      $scope.$on('$ionicView.beforeEnter', function () {
        initModule();
      });

    })

;
