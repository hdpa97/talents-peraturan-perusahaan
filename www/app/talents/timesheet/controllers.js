angular.module('timesheet.controllers', [])

  .controller('DetailTimesheetCtrl',
    function ($ionicLoading) {

    })

  .controller('ListTimesheetCtrl',
    function ($ionicLoading) {

    })


  .controller('CalendarTimesheetCtrl',
    function (
      $scope
    ) {

      initCalendar();

      function initCalendar() {
        $scope.calendarView = 'week';
        $scope.viewDate = new Date();
        $scope.events = $scope.notifications;

        $scope.eventClicked = function (event) {
          //alert.show('Clicked', event);
        };

        $scope.eventEdited = function (event) {
          //alert.show('Edited', event);
        };

        $scope.eventDeleted = function (event) {
          //alert.show('Deleted', event);
        };

        $scope.eventTimesChanged = function (event) {
          //alert.show('Dropped or resized', event);
        };

        $scope.toggle = function ($event, field, event) {
          $event.preventDefault();
          $event.stopPropagation();
          event[field] = !event[field];
        };

        $scope.viewChangeClicked = function (nextView, date) {
          $scope.viewDate = date;
          $scope.seletedDateEvents = [];
          if (nextView === 'day') {
            angular.forEach($scope.events, function (value, key) {
              var range = moment().range(value.startsAt, value.endsAt);
              if (range.contains(date)) {
                $scope.seletedDateEvents.push(value);
              }
            });
            return false;
          }
        };

        $scope.getDayEvents = function () {
          $scope.selectedDate = new Date();
          angular.forEach($scope.events, function (value, key) {
            var range = moment().range(value.startsAt, value.endsAt);
            if (range.contains(new Date())) {
              $scope.seletedDateEvents.push(value);
            }
          });
        }

      }
    })

  .controller('AddTimesheetCtrl',
    function (
      ionicDatePicker,
      ionicTimePicker,
      $scope
    ) {

      $scope.leaveType = {};
      $scope.leave = {
        remark: ""
      };
      $scope.leave.startDate = new Date();
      $scope.shift = ["Morning", "Afternoon", "Evening", "Night"];


      var datepicker = {
        callback: function (val) { //Mandatory
          $scope.leave.startDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: "yyyy-MM-dd",
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };


      $scope.openDatePicker = function () {
        ionicDatePicker.openDatePicker(datepicker);
      };



      $scope.time = {
        remark: ""
      };
      $scope.time.start = new Date();


      var ipObj1 = {
        callback: function (val) { //Mandatory
          $scope.time.start = new Date(val * 1000);
          var hours = parseInt(val / 3600);
          var minutes = (val / 60) % 60;
          if (typeof (val) === 'undefined') {

          } else {

          }
        },
        inputTime: 50400, //Optional
        format: 24, //Optional
        step: 5, //Optional
        setLabel: 'Set' //Optional
      };

      $scope.openTimePicker = function () {
        ionicTimePicker.openTimePicker(ipObj1);
      };
    })

    ;